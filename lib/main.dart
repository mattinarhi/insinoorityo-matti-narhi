
import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:inssityo/Database.dart';
import 'package:inssityo/appLocalization.dart';
import 'package:inssityo/languageProvider.dart';
import 'package:inssityo/orderInformation.dart';
import 'package:inssityo/product.dart';
import 'dart:core';
import 'package:inssityo/startScreen.dart';
import 'package:inssityo/themeNotifier.dart';
import 'package:inssityo/user.dart';
import 'package:provider/provider.dart';

void main() async {

  WidgetsFlutterBinding.ensureInitialized();

  LanguageProvider provider = LanguageProvider();

  await provider.fetchLocale();


  runApp(

   MyApp(provider));

List<User> users = await SQLiteDbProvider.db.getAllUsers();

final orders = await SQLiteDbProvider.db.getAllOrders();

print(orders);

//print(users);
  
}

class MyApp extends StatelessWidget {

  final LanguageProvider provider;

  MyApp(this.provider);



  @override
  Widget build(BuildContext context) {

    return ChangeNotifierProvider(

      create: (_) => ThemeNotifier(),

      child: Consumer<ThemeNotifier>(

      builder: (context, provider, child) {

      return ChangeNotifierProvider<LanguageProvider>(

        create: (_) => this.provider,

        child: Consumer<LanguageProvider>(

          builder: (context, notifier, child) {


            return
        
      


      



      MaterialApp(

       theme: provider.darkTheme ? provider.dark : provider.light,

       locale: notifier.appLocal,

       supportedLocales: [Locale('en', 'US'), Locale('fi', '')],

       localizationsDelegates: [

         AppLocalizations.delegate,

         GlobalMaterialLocalizations.delegate,

         GlobalWidgetsLocalizations.delegate
       ],
     
      
      home: StartScreen(),
    );
  }));
}));
}
}