import 'package:flutter/material.dart';
import 'package:inssityo/Database.dart';
import 'package:inssityo/appLocalization.dart';
import 'package:inssityo/settings.dart';
import 'package:inssityo/user.dart';
import 'package:inssityo/validator.dart';

class SignUpScreen extends StatefulWidget {

  @override
  _SignUpScreenState createState() => _SignUpScreenState();
}

class _SignUpScreenState extends State<SignUpScreen> {

  final _formKey = GlobalKey<FormState>();

  Validator validator = new Validator();

  User user;

  String userName;

  String password1;

  String password2;

  String firstName;

  String lastName;

  String address;

  String postalCode;

  String city;

  String phoneNumber;

  String email;

  String dob;

  int isAdmin = 0;

  List<User> users;




  


  @override
  Widget build(BuildContext context) {
    return Scaffold(

        resizeToAvoidBottomInset: true,
        appBar: AppBar(actions: [IconButton(onPressed: () {

        Navigator.push(context, MaterialPageRoute(builder: (context) => Settings()));

      }

      , icon: Icon(Icons.settings))],),
      
      body: SingleChildScrollView(

      reverse: true,
      
      scrollDirection: Axis.vertical,
        
      child: FutureBuilder(future: SQLiteDbProvider.db.getAllUsers(),
      builder: (context, AsyncSnapshot<List<User>> snapshot) {

    if(snapshot.hasData) {

    List<User> users = snapshot.data;

      
    return  Form(
          key: _formKey,
          autovalidateMode: AutovalidateMode.always,
          child: Column(
            children: [ 
              TextFormField(onChanged: (value)=> userName = value,
              validator: (value) => validator.userNameValidator(value, users, context),
              
              decoration: InputDecoration(hintText: AppLocalizations.of(context).translate("username")),
               
               
               ),
             
              TextFormField(onChanged: (value) => password1 = value,
              decoration: InputDecoration(hintText: AppLocalizations.of(context).translate("password")),
              validator: (value) => validator.passwordValidator(value, context),
              obscureText: true,),
           
              TextFormField(onChanged: (value) => password2 = value,
              decoration: InputDecoration(hintText: AppLocalizations.of(context).translate("password_check")),
              validator: (value) => validator.passwordMatchCheck(value, password1, context),
              obscureText: true,),

              
              Row(children: [
                Expanded(child:
                
                TextFormField(onChanged: (value) => firstName = value,
                decoration: InputDecoration(hintText: AppLocalizations.of(context).translate("first_name")),
                validator: (value) => validator.nameValidator(value, context),)),

                Expanded(child: 
                TextFormField(onChanged: (value) => lastName = value,
                decoration: InputDecoration(hintText: AppLocalizations.of(context).translate("last_name")),
                validator: (value) => validator.nameValidator(value, context),))]),
              
             
              TextFormField(onChanged: (value) => address = value,
              decoration: InputDecoration(hintText: AppLocalizations.of(context).translate("address")),
              validator: (value) => validator.addressValidator(value, context),),
              Row(
                children: [
                  Expanded(child: TextFormField(onChanged: (value) => postalCode = value,
                  decoration: InputDecoration(hintText: AppLocalizations.of(context).translate("postal_code")),
                  validator: (value) => validator.postalCodeValidator(value, context),)),

                  
                Expanded(child: TextFormField(onChanged: (value) => city = value,
                  decoration: InputDecoration(hintText: AppLocalizations.of(context).translate("city")),
                  validator: (value) => validator.cityNameValidator(value, context),))
                ],
              ),

              
              Row(
                children: [
                 
                  Expanded(child: TextFormField(onChanged: (value) => phoneNumber = value,
                  decoration: InputDecoration(hintText: AppLocalizations.of(context).translate("phone_number")),
                  validator: (value) => validator.phoneNumberValidator(value, context),)),

                 
                  Expanded(child: TextFormField(onChanged: (value) => email = value,
                  decoration: InputDecoration(hintText: AppLocalizations.of(context).translate("email")),
                  validator: (value) => validator.validateEmail(value, context)
                  ))]
              ),

              

              TextFormField(onChanged: (value) => dob = value,
              decoration: InputDecoration(hintText: AppLocalizations.of(context).translate("dob")),
              validator: (value) => validator.dobValidator(value, context)),
              

                 Row(
          children: [
        ElevatedButton(onPressed: () => {
          Navigator.pop(context)
        },
        style: ElevatedButton.styleFrom(primary: Colors.grey),
        child: Text(AppLocalizations.of(context).translate("cancelBtn")),
                 ),

        ElevatedButton(
          onPressed: () async => {

            if(_formKey.currentState.validate() == true) {
            user = new User(0, userName, password2, firstName, lastName, address, postalCode, city, phoneNumber, email, dob, isAdmin),
            Navigator.pop(context),
            ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text(AppLocalizations.of(context).translate("user_created")))),
            await SQLiteDbProvider.db.insert(user),
            print(await SQLiteDbProvider.db.getAllUsers())

            }

            else {

              ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text(AppLocalizations.of(context).translate("signup_error")))),

            }
          },
          child: Text(AppLocalizations.of(context).translate("sign_up")),
          style: ElevatedButton.styleFrom(primary: Colors.green),
          ),
            ])]),

            );

    

   
    

    
    
  
    
    }

    else {

      return Center(child: CircularProgressIndicator());
    }

   
      
    
  
  }
  
  
  )));
      
  
  }
}