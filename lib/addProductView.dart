import 'dart:async';
import 'dart:ffi';
import 'dart:io';
import 'dart:typed_data';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:inssityo/Database.dart';
import 'package:inssityo/appLocalization.dart';
import 'package:inssityo/pictureWidget.dart';
import 'package:inssityo/product.dart';
import 'package:inssityo/validator.dart';

class AddProductView extends StatefulWidget {

  final List<Product> products;

  AddProductView(this.products);


  @override
  _AddProductViewState createState() => _AddProductViewState();
}

class _AddProductViewState extends State<AddProductView> {

  PictureWidget pictureWidget = PictureWidget();

  final _formKey = GlobalKey<FormState>();

  int id;

  String name;

  String description;

  String category;

  double price;

  Validator validator = Validator();

  List<String> categories = ["Practice", "Clothes", "Shoes", "Equipment", "Other"];

  String selectedCategory;

  @override
  void initState() {

    super.initState();

    selectedCategory = categories[0];
  }

  


  @override
  Widget build(BuildContext context) {
    return Scaffold(

      appBar: AppBar(title: Text(AppLocalizations.of(context).translate("add_product")),),
      resizeToAvoidBottomInset: false,

      body: SingleChildScrollView(

        scrollDirection: Axis.vertical,
        
        child: Column(


          children: [

            pictureWidget,

            Align(child: buildDropdownMenu(), alignment: Alignment.centerLeft,),

            Form(

              key: _formKey,

              autovalidateMode: AutovalidateMode.always,
              
              
              child: 

              Column(

                
                
                children: [

                TextFormField(onChanged: (value) => name = value,
                decoration: InputDecoration(hintText: AppLocalizations.of(context).translate("product_name")),
                validator: (value) => validator.productNameValidator(value, widget.products, context),),

                TextFormField(onChanged: (value) => description = value,
                decoration: InputDecoration(hintText: AppLocalizations.of(context).translate("product_description")),
                validator: (value) => validator.descriptionValidator(value, context),),


                TextFormField(onChanged: (value) => price = double.tryParse(value),
                decoration: InputDecoration(hintText: AppLocalizations.of(context).translate("product_price")),
                validator: (value) => validator.priceValidator(value, context),),

                Center(child: Row(children: [

                  ElevatedButton(onPressed: () {

                    Navigator.pop(context);


                  }, child: Text(AppLocalizations.of(context).translate("cancelBtn")),
                  
                  style: ElevatedButton.styleFrom(primary: Colors.grey),),

                  ElevatedButton(onPressed: () async {

                    ByteData img = await rootBundle.load("lib/assets/images/flutter.png");

                    Uint8List imgBytes = img.buffer.asUint8List();

                    if(_formKey.currentState.validate() == true && PictureWidget.imgAdded == true) {

                      Uint8List img =  PictureWidget.img.readAsBytesSync();

                      Product product = new Product(id, name, description, selectedCategory, price, img);

                      await SQLiteDbProvider.db.insertProduct(product);

                      Navigator.pop(context);

                      ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text(AppLocalizations.of(context).translate("with_picture"))));
                    }

                      else if(_formKey.currentState.validate() == true && PictureWidget.imgAdded == false) {

                          Product product = new Product(id, name, description, selectedCategory, price, imgBytes);

                          await SQLiteDbProvider.db.insertProduct(product);

                          Navigator.pop(context);

                          ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text(AppLocalizations.of(context).translate("without_picture"))));





                      }

                      else {

                        ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text(AppLocalizations.of(context).translate("error_msg"))));
                      }







                  }, child: Text(AppLocalizations.of(context).translate("addBtn")),
                  
                  style: ElevatedButton.styleFrom(primary: Colors.green),)


                ],),)


              ],)
            
            
            ,)
          ],


        )),
      
    );
  }

  Widget buildDropdownMenu() {


    return DropdownButton(

      hint: Text(AppLocalizations.of(context).translate("choose_category")),
      value: selectedCategory,
      onChanged: (value) {

        setState(() {
          
          selectedCategory = value;
        });
      },

      items: categories.map((_category) {

        return DropdownMenuItem(

          child: Text(_category),
          value: _category
        );


      }).toList()


    );
  }

 
  
}