import 'package:flutter/material.dart';
import 'package:inssityo/appLocalization.dart';
import 'package:inssityo/languageProvider.dart';
import 'package:inssityo/themeNotifier.dart';
import 'package:provider/provider.dart';

class Settings extends StatefulWidget {

  @override
  _SettingsState createState() => _SettingsState();
}

class _SettingsState extends State<Settings> {
  @override
  Widget build(BuildContext context) {

    var appLanguage = Provider.of<LanguageProvider>(context);
    return Scaffold(
      appBar: AppBar(title: Text(AppLocalizations.of(context).translate("settings")),
      ),
      body: Column(mainAxisAlignment: MainAxisAlignment.center,
      children: [Center(child: Consumer<ThemeNotifier>(

        builder: (context, provider, child) {

          return SwitchListTile(value: provider.darkTheme, onChanged: (value) {

            provider.toggleTheme();
          },
          
          title: Text(AppLocalizations.of(context).translate("dark_mode_settings")));


        },



      )), Center(child: SwitchListTile(value: appLanguage.appLocal == Locale("en") ? false : true, onChanged: (value) {

        value == true ? appLanguage.changeLanguage(Locale("fi")) : appLanguage.changeLanguage(Locale("en"),
        );
      }, title: Text(AppLocalizations.of(context).translate("language_settings")),),)],),
      
    );
  }
}