import 'package:flutter/material.dart';
import 'package:inssityo/Database.dart';
import 'package:inssityo/appLocalization.dart';
import 'package:inssityo/order.dart';
import 'package:inssityo/orderRow.dart';
import 'package:inssityo/product.dart';
import 'package:inssityo/productDetailScreen.dart';

class OrderDetailScreen extends StatefulWidget {

  final Order order;



  OrderDetailScreen(this.order);

  

  @override
  _OrderDetailScreenState createState() => _OrderDetailScreenState();
}

class _OrderDetailScreenState extends State<OrderDetailScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(

      appBar: AppBar(title: Text(AppLocalizations.of(context).translate("order_information")),),

      body: FutureBuilder(

        future: Future.wait([SQLiteDbProvider.db.getRowsByOrderId(widget.order.id), SQLiteDbProvider.db.getOrderProducts(widget.order.id)]),

        builder: (context, AsyncSnapshot<List<dynamic>> snapshot) {

          if(snapshot.hasData) {

            List<Product> products = snapshot.data[1];

            return Column(children: [

              Text(AppLocalizations.of(context).translate("order_number") + ": " + widget.order.id.toString()),

              Text(AppLocalizations.of(context).translate("order_date") + ": " + widget.order.dateTime),

              Text(AppLocalizations.of(context).translate("total_price") + ": " + widget.order.price.toStringAsFixed(2)),

              Text(AppLocalizations.of(context).translate("items") + countOrderSize(snapshot.data[0]).toString()),

              Text(AppLocalizations.of(context).translate("state") + ": " + widget.order.state),

              Text(AppLocalizations.of(context).translate("delivery_method") + ": " + widget.order.deliveryMethod),


             Expanded(child: ListView.builder(

                itemCount: products.length,
                itemBuilder: (context, index) {

                  return Card(

                    child: InkWell(

                      onTap: () {

                        Navigator.push(context, MaterialPageRoute(builder: (context) => ProductDetailScreen(products[index])));


                      },

                    child: Container(

                      child: Column(children: [

                        Container(

                          height: 100,

                          width: 100,

                          child: Image.memory(products[index].picture),
                        ),

                        Text(AppLocalizations.of(context).translate("product_name") + ": " + products[index].name),

                        Text(AppLocalizations.of(context).translate("product_price") + ": " + products[index].price.toStringAsFixed(2)),

                        getProductAmount(products[index].id, snapshot.data[0])


                      ],)

                    ),
                    ),
                  );


                },




              ),



            )],);



          }

          else {

            return CircularProgressIndicator();
          }




        },

      


      ),
      
    );
  }

  int countOrderSize(List<OrderRow> rows) {

    int size = 0;

    for (int i = 0; i < rows.length; i++) {

      size = size + rows[i].itemCount;


    }

    return size;


    
  }

  Widget getProductAmount(int productId, List<OrderRow> rows) {

    final index = rows.indexWhere((element) => element.productId == productId);

    return Text(AppLocalizations.of(context).translate("amount") + ": " + rows[index].itemCount.toString());
  }
}