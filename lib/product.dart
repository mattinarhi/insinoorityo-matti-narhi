import 'dart:typed_data';

class Product {

  int id;

  String name;

  String description;

  String category;

  double price;

  int itemCount = 0;

  Uint8List picture;

  Product(this.id, this.name, this.description, this.category, this.price, this.picture);

  factory Product.fromMap(Map<String, dynamic> data) {

    return Product(

      data['id'],
      data['name'],
      data['description'],
      data['category'],
      data['price'],
      data['picture']
    );
  }

  Map<String, dynamic> toMap() => {

    "id": id,

    "name": name,

    "description": description,

    "category": category,

    "price": price,

    "picture": picture



  };


  @override 
  String toString() {

    return "Product Id: " + id.toString() + " Name: " + name + " Description: " + description + " Category: " + category + " Price: " + price.toString(); 
  }




}