import 'package:flutter/material.dart';
import 'package:inssityo/appLocalization.dart';
import 'package:inssityo/loginScreen.dart';
import 'package:inssityo/settings.dart';
import 'package:inssityo/signupScreen.dart';

class StartScreen extends StatefulWidget {

  @override
  _StartScreenState createState() => _StartScreenState();
}

class _StartScreenState extends State<StartScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(

      appBar: AppBar(actions: [IconButton(onPressed: () {

        Navigator.push(context, MaterialPageRoute(builder: (context) => Settings()));

      }

      , icon: Icon(Icons.settings))],),

      body: Center(child:
      Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [

          ElevatedButton(onPressed: () => {
            Navigator.push(context, MaterialPageRoute(builder: (context) => LogInScreen()))
          },
          style: ElevatedButton.styleFrom(primary: Colors.blue),
          child: Text(AppLocalizations.of(context).translate("log_in"))),

          ElevatedButton(onPressed: () => {
            Navigator.push(context, MaterialPageRoute(builder: (context) => SignUpScreen()))
          },
          style: ElevatedButton.styleFrom(primary: Colors.green),
          child: Text(AppLocalizations.of(context).translate("sign_up")))


      ],)
    ));
      
    
  }
}