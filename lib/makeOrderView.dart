import 'package:flutter/material.dart';
import 'package:inssityo/Database.dart';
import 'package:inssityo/adminOrderView.dart';
import 'package:inssityo/appLocalization.dart';
import 'package:inssityo/cartProvider.dart';
import 'package:inssityo/product.dart';
import 'package:inssityo/productDetailScreen.dart';
import 'package:inssityo/user.dart';
import 'package:inssityo/userOrderView.dart';
import 'package:provider/provider.dart';

class MakeOrderView extends StatefulWidget {

  final String username;

  final List<Product> products;

  final double totalPrice;

  final int cartSize;

  MakeOrderView(this.username,this.products, this.totalPrice, this.cartSize);

  @override
  _MakeOrderViewState createState() => _MakeOrderViewState();
}

class _MakeOrderViewState extends State<MakeOrderView> {

 

  String deliveryMethod = "Post";


  @override
  Widget build(BuildContext context) {
    return Scaffold(

      appBar: AppBar(title: Text(AppLocalizations.of(context).translate("order_title")),),

      body: FutureBuilder(

        future: SQLiteDbProvider.db.getUserByUserName(widget.username),
        builder: (context, AsyncSnapshot<User> snapshot) {


        if(snapshot.hasData) {





      

       return  Column(mainAxisAlignment: MainAxisAlignment.center,
          children: [

            Align(alignment: Alignment.topCenter, child: Text(AppLocalizations.of(context).translate("order_information"), style: TextStyle(fontSize: 20.0, decoration: TextDecoration.underline))),



            Align(alignment: Alignment.topLeft, child: Text(AppLocalizations.of(context).translate("orderer_name") + ": " + snapshot.data.firstName + " " + snapshot.data.lastName, style: TextStyle(fontSize: 20.0))),

            Align(alignment: Alignment.topLeft, child: Text(AppLocalizations.of(context).translate("billing_address") + ": " + snapshot.data.address + " " + snapshot.data.postalCode + " " + snapshot.data.city, style: TextStyle(fontSize: 20.0),),),

            Align(alignment: Alignment.topLeft, child: Text(AppLocalizations.of(context).translate("items") + ": " + widget.cartSize.toString(), style: TextStyle(fontSize: 20.0),),),


            Align(alignment: Alignment.topLeft, child: Text(AppLocalizations.of(context).translate("total_price") + ": " + widget.totalPrice.toStringAsFixed(2) + " €", style: TextStyle(fontSize: 20.0),),),

            Text(AppLocalizations.of(context).translate("products"), style: TextStyle(fontSize: 20.0, decoration: TextDecoration.underline),),

            Container(

              height: MediaQuery.of(context).size.height/3,
              
              width: MediaQuery.of(context).size.width, child: ListView.builder(

              itemCount: widget.products.length,

              itemBuilder: (context, index) {

                return Card(child: 
                
                InkWell(

                  onTap: () {

                    Navigator.push(context, MaterialPageRoute(builder: (context) => ProductDetailScreen(widget.products[index])));





                  },

                  child: Column(

                    children: [
                      Container(

                        height: 100,
                        width: MediaQuery.of(context).size.width,
                        child: Image.memory(widget.products[index].picture),

                      ),

                      Text(AppLocalizations.of(context).translate("product_name") + ": " + widget.products[index].name),
                      Text(AppLocalizations.of(context).translate("product_price") + ": " + widget.products[index].price.toStringAsFixed(2) + " €"),
                      Text(AppLocalizations.of(context).translate("amount_ordered") + ": " + widget.products[index].itemCount.toString())

                    
                    ],
                  ),


                ));




              },




            )),

            Padding(padding: EdgeInsets.only(top: 30.0), child: Text(AppLocalizations.of(context).translate("delivery_method"), style: TextStyle(fontSize: 20.0, decoration: TextDecoration.underline,))),


            Container(

              height: 50,
              
              width:MediaQuery.of(context).size.width, 
            child: Row(children: [

             Expanded(child: ListTile(
                title: Text(AppLocalizations.of(context).translate("post")),
                leading: Radio(

                  value: "Post",
                  groupValue: deliveryMethod,
                  onChanged: (value) {

                    setState(() {
                      
                      deliveryMethod = value;
                    });


                  },


                ),
              ),

 

            ),
            
                         Expanded(child: ListTile(

                title: Text(AppLocalizations.of(context).translate("fetch")),

                leading: Radio(

                  value: "Fetch from studio",

                  groupValue: deliveryMethod,

                  onChanged: (value) {

                    setState(() {
                      
                      deliveryMethod = value;
                    });
                  },
                )


              ))],)),


           

              Row(children: [

              Align(alignment: Alignment.bottomCenter,

              child:

              ElevatedButton(onPressed: () {

                Navigator.pop(context);



              }, child: Text(AppLocalizations.of(context).translate("cancelBtn")), style: ElevatedButton.styleFrom(primary: Colors.grey),)),

              Align(

              alignment: Alignment.bottomCenter,

              child:
            
            
              ElevatedButton(onPressed: () async {
              
              await SQLiteDbProvider.db.insertOrder(snapshot.data.id, widget.products, widget.totalPrice, deliveryMethod);


              Navigator.pop(context, true);

           
              
            }, child: Text(AppLocalizations.of(context).translate("orderBtn")),
            style: ElevatedButton.styleFrom(primary: Colors.green),))])
       ]);
        


        }

        else {

          return CircularProgressIndicator();
        }
        }
      
    ));
  }
}