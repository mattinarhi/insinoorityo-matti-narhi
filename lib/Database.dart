import 'dart:async'; 
import 'dart:io';
import 'dart:typed_data'; 
import 'package:flutter/services.dart';
import 'package:inssityo/order.dart';
import 'package:inssityo/orderInformation.dart';
import 'package:inssityo/orderRow.dart';
import 'package:inssityo/product.dart';
import 'package:inssityo/user.dart';
import 'package:path/path.dart'; 
import 'package:path_provider/path_provider.dart'; 
import 'package:sqflite/sqflite.dart';

class SQLiteDbProvider {

  SQLiteDbProvider._();
  static final SQLiteDbProvider db = SQLiteDbProvider._();
  static Database _database;

  Future<Database> get database async {

    if(_database != null)
    return _database;

    _database = await initDB();
    return _database;


  }

  initDB() async {

    Directory documentsDirectory = await getApplicationDocumentsDirectory();

  
    final ByteData bytes = await rootBundle.load("lib/assets/images/flutter.png");

    final Uint8List image = bytes.buffer.asUint8List();

    String path = join(documentsDirectory.path, "AppDB.db");

    return await openDatabase(
      path, version: 1,
      onOpen: (db) {},
      onCreate: (Database db, int version) async {

        await db.execute('PRAGMA foreign_keys = ON');

        await db.execute(
        "CREATE TABLE User ("
        "id INTEGER PRIMARY KEY,"
        "userName TEXT,"
        "password TEXT,"
        "firstName TEXT,"
        "lastName TEXT,"
        "address TEXT,"
        "postalCode TEXT,"
        "city TEXT,"
        "phoneNumber TEXT,"
        "email TEXT,"
        "dob TEXT,"
        "isAdmin NUMERIC"")"

        

        );

        await db.execute(
          "INSERT INTO User"
          "('id', 'userName', 'password', 'firstName', 'lastName', 'address', 'postalCode', 'city', 'phonenumber', 'email', 'dob', 'isAdmin')"
          "values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)",
          [1, 'admin', 'admin', 'Matti', 'Närhi', 'Karhulantie 13 B 43', '00950', 'Helsinki', '0400416220', 'matti.narhi2@metropolia.fi', '03.09.1998', 1]
        
        
        );

        await db.execute(

          "CREATE TABLE Product ("
          "id INTEGER PRIMARY KEY,"
          "name TEXT,"
          "description TEXT,"
          "category TEXT,"
          "price REAL,"
          "picture BLOB"")"

        );

        await db.execute(

          "INSERT INTO Product"
          "('id', 'name', 'description', 'category', 'price', 'picture')"
          "values (?, ?, ?, ?, ?, ?)",
          [1, 'Test product', 'This is a test product', 'Other', '10.00', image]
        );

        await db.execute(
          "INSERT INTO Product"
          "('id', 'name', 'description', 'category', 'price', 'picture')"
          "values (?, ?, ?, ?, ?, ?)",
          [2, 'Practice card 10 times', 'Practice card 10 times', 'Practice', '40.00', image]

        );

        await db.execute(

          "CREATE TABLE Orders ("
          "id INTEGER PRIMARY KEY,"
          "price REAL,"
          "userId INTEGER,"
          "state TEXT,"
          "deliveryMethod TEXT,"
          "dateTime TEXT,"
          "FOREIGN KEY (userId) REFERENCES User (id)"")"

          

        );

        await db.execute(

          "INSERT INTO Orders"
          "('id', 'price', 'userId', 'state', 'deliveryMethod', 'dateTime')"
          "VALUES (?, ?, ?, ?, ?, ?)",
          [1, 50.00, 1, 'Open', 'Post', 'datetime()']


        );

        await db.execute(

          "CREATE TABLE OrderRows ("
          "id INTEGER PRIMARY KEY,"
          "orderId INTEGER,"
          "productId INTEGER,"
          "itemCount INTEGER,"
          "FOREIGN KEY (orderId) REFERENCES Orders (id) ON DELETE NO ACTION ON UPDATE NO ACTION,"
          "FOREIGN KEY (productId) REFERENCES Product (id) ON DELETE NO ACTION ON UPDATE NO ACTION"")"
        );

        await db.execute(

          "INSERT INTO OrderRows"
          "('id', 'orderId', 'productId', 'itemCount')"
          "VALUES (?, ?, ?, ?)",
          [1, 1, 1, 1]
        );


        await db.execute(

          "INSERT INTO OrderRows"
          "('id', 'orderId', 'productId', 'itemCount')"
          "VALUES (?, ?, ?, ?)",
          [2, 1, 2, 1]
        );



      }
    );



  }

    Future<List<User>> getAllUsers() async {

    final db = await database;

    List<Map> results = await db.query("User");

    List<User> users = results.isNotEmpty ? results.map((e) => User.fromMap(e)).toList() : [];

    
  

  return users;


  }

  Future<User> getUserByUserName(String userName) async {

    final db = await database;

    var result = await db.query("User", where: "userName = ?", whereArgs: ["$userName"]);

    return result.isNotEmpty ? User.fromMap(result.first) : Null;


  }

  insert(User user) async {

    final db = await database;

    var maxId = await db.rawQuery(
      "SELECT MAX(id)+1 as last_inserted_id FROM User"
    );

    var id = maxId.first["last_inserted_id"];

    var result = db.rawInsert(
      "INSERT INTO User"
      "('id', 'userName', 'password', 'firstName', 'lastName', 'address', 'postalCode', 'city', 'phonenumber', 'email', 'dob', 'isAdmin')"
      "values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)",
       [id, user.userName, user.password, user.firstName, user.lastName, user.address, user.postalCode, user.city, user.phoneNumber, user.email, user.dob, user.isAdmin]
    );

    return result;

  }

  updateUser(User user) async{

    final db = await database;

    var result = db.update("User", user.toMap(), where: "id = ?", whereArgs: [user.id]);

    return result;



  }

  deleteUser(int id) async{


    final db = await database;

    var result = db.delete("User", where: "id = ?", whereArgs: [id]);

    return result;
  }

  Future<List<Product>> getAllProducts() async {


    final db = await database;

    List<Map> results = await db.query("Product", orderBy: "id DESC");

    List<Product> products = results.isNotEmpty ? results.map((e) => Product.fromMap(e)).toList() : [];

    return products;


  }


  deleteProduct(int id) async {

    final db = await database;

    var result = db.delete("Product", where: "id = ?", whereArgs: [id]);

  

    return result;
  }

  insertProduct(Product product) async{

    final db = await database;

    var maxId = await db.rawQuery("SELECT MAX(id)+1 as last_inserted_id FROM Product");

    var id = maxId.first["last_inserted_id"];

    var result = db.rawInsert(

      "INSERT INTO Product"
      "('id', 'name', 'description', 'category', 'price', 'picture')"
      "values (?, ?, ?, ?, ?, ?)",
      [id, product.name, product.description, product.category, product.price, product.picture]

    );

    return result;


  }

 Future<List<Product>> getOrderProducts(int orderId) async{

    final db = await database;



  
    List<Product> products = [];



        
 List<Map<String,dynamic>> dbList = await db.rawQuery("SELECT * FROM OrderRows INNER JOIN Product ON OrderRows.productId = Product.id WHERE OrderRows.orderId = $orderId");

   

    dbList.forEach((element) {

      products.add(Product.fromMap(element));


    });



    return products;

    

  }


  insertOrder(int userId, List<Product> products, double totalPrice, String deliveryMethod) async {

    final db = await database;

    var maxId = await db.rawQuery("SELECT MAX(id)+1 as last_inserted_id FROM Orders");

    var id = maxId.first['last_inserted_id'];

    var datetime = DateTime.now().toString();



    await db.rawInsert(

      "Insert INTO Orders"
      "('id', 'price', 'userId', 'state', 'deliveryMethod', 'dateTime')"
      "VALUES (?, ?, ?, ?, ?, ?)",
      [id, totalPrice, userId, 'Open', deliveryMethod, datetime]
    );

    for(int i = 0; i < products.length; i++) {

      await insertOrderRows(id, products[i]);
    }




  }


  insertOrderRows(int orderId, Product product) async {

    final db = await database;

    var maxId = await db.rawQuery("SELECT MAX(id)+1 as last_inserted_id FROM OrderRows");

    var id = maxId.first['last_inserted_id'];


    await db.rawInsert(

      "Insert INTO OrderRows"
      "('id', 'orderId', 'productId', 'itemCount')"
      "VALUES (?, ?, ?, ?)",
      [id, orderId, product.id, product.itemCount]
    );

    id++;




  }

 Future<List<Order>> getAllOrders() async {

    final db = await database;

    List<Map> results = await db.query("Orders", orderBy: "id DESC");

    List<Order> orders = results.isNotEmpty ? results.map((e) => Order.fromMap(e)).toList() : [];

    return orders;

    
  }

 Future<List<Order>> getUserOrders(String username) async {

   final db = await database;


   var result = await db.rawQuery("SELECT id as user_id FROM User WHERE userName ='$username'");

   var id = result.first['user_id'];

   List<Map> query = await db.query("Orders", where: "userId = ?", whereArgs: [id], orderBy: "id DESC");

   List<Order> orders = query.isNotEmpty ? query.map((e) => Order.fromMap(e)).toList() : [];

   return orders;
 }

 updateProduct(Product product) async {

   final db = await database;

   await db.update("Product", product.toMap(), where: "id = ?", whereArgs: [product.id]);


 }

 updateOrder(Order order) async {

   final db = await database;

   await db.update("Orders", order.toMap(), where: "id = ?", whereArgs: [order.id]);


 }


 Future<List<OrderRow>> getRowsByOrderId(int orderId) async {

   final db = await database;

   List<Map> result = await db.query("OrderRows", where: "orderId = ?", whereArgs: [orderId]);

   List<OrderRow> rows = result.isNotEmpty ? result.map((e) => OrderRow.fromMap(e)).toList() : [];

   return rows;
     
   


 }



  



}