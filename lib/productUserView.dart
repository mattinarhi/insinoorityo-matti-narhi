
import 'package:flutter/material.dart';
import 'package:inssityo/Database.dart';
import 'package:inssityo/appLocalization.dart';
import 'package:inssityo/cartProvider.dart';
import 'package:inssityo/product.dart';
import 'package:inssityo/productDetailScreen.dart';
import 'package:provider/provider.dart';

class ProductUserView extends StatefulWidget {

  @override
  _ProductUserViewState createState() => _ProductUserViewState();
}

class _ProductUserViewState extends State<ProductUserView> {

  var cartProvider;

  List<Product> duplicateItems = [];

  List<Product> items = [];

  @override
  void initState() {

    super.initState();

    fetchProductData().whenComplete(() {

      items.addAll(duplicateItems);
    });

     

    items.addAll(duplicateItems);

    cartProvider = new CartProvider();


  }

  Future<void> fetchProductData() async{

  duplicateItems = await SQLiteDbProvider.db.getAllProducts();

    

  }

  void filterSearchResults(String query) {

    List<Product> dummySearchList = [];

    dummySearchList.addAll(duplicateItems);

    if(query.isNotEmpty) {

      List<Product> dummyListData = [];

      dummySearchList.forEach((element) {

        if(element.name.contains(query)) {

          dummyListData.add(element);
        }

      });

      setState(() {
        items.clear();
        items.addAll(dummyListData);
      });
    }

    else {

      setState(() {
        
        items.clear();
        items.addAll(duplicateItems);
      });
    }


  }



  @override
  Widget build(BuildContext context) {



  

   
return
    

       Scaffold(

      appBar: AppBar(title: Text(AppLocalizations.of(context).translate("products")),
      automaticallyImplyLeading: false,),

      body:  FutureBuilder(

        future: SQLiteDbProvider.db.getAllProducts(),
        builder: (context, AsyncSnapshot<List<Product>> snapshot) {

          if(snapshot.hasData) {

      
        
            
            return Consumer<CartProvider>(


          builder: (context, provider, child) {


            
            
          return Column(
            
            children: [
            
            TextField(
              onChanged: (value) {

                filterSearchResults(value);

                
                
              },
              decoration: InputDecoration(prefixIcon: Icon(Icons.search)),
            ),
          
          Expanded(child: 
           
           ListView.builder(
            
            scrollDirection: Axis.vertical,
              
            itemCount:items.length,

            itemBuilder: (context, index) {


              return Card(child: 
              InkWell(

                onTap: () {

                Navigator.push(context, MaterialPageRoute(builder: (context) => ProductDetailScreen(items[index])));


                },
                
                
                
                child: Column(children: [

                  Container(
                    height: 150,
                    width: MediaQuery.of(context).size.width,
                    child: Image.memory(items[index].picture),
                  ),

                  Text(items[index].name, style: TextStyle(fontSize: 20.0, fontWeight: FontWeight.bold)),

                  Text(AppLocalizations.of(context).translate("price") + ": " + items[index].price.toStringAsFixed(2) + " €", style: TextStyle(fontSize: 20.0, fontWeight: FontWeight.bold)),

                  ElevatedButton(onPressed: () {

                    provider.addProduct(items[index]);

                    print(provider.cartSize);

                   





                  }, child: Text("+"),
                  style: ElevatedButton.styleFrom(primary: Colors.green),)


                ],)));

                

            
          }))]);});
          }

          else {

            return CircularProgressIndicator();
          }
        },

   
      ));
   

      }
      
      
      

  }
