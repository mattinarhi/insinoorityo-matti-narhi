import 'package:flutter/material.dart';
import 'package:inssityo/Database.dart';
import 'package:inssityo/adminMainScreen.dart';
import 'package:inssityo/appLocalization.dart';
import 'package:inssityo/settings.dart';
import 'package:inssityo/user.dart';
import 'package:inssityo/userMainScreen.dart';

class LogInScreen extends StatefulWidget {

  @override
  _LogInScreenState createState() => _LogInScreenState();
}

class _LogInScreenState extends State<LogInScreen> {

  final _formKey = GlobalKey<FormState>();

  String username;
  String password;

  @override
  Widget build(BuildContext context) {
     return Scaffold(

        appBar: AppBar(actions: [IconButton(onPressed: () {

        Navigator.push(context, MaterialPageRoute(builder: (context) => Settings()));

      }

      , icon: Icon(Icons.settings))],),

      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [Form(
          key: _formKey,
          child: Column(children: [
            TextFormField(onChanged: (value) => username = value,
            decoration: InputDecoration(hintText: AppLocalizations.of(context).translate("username")),),

            TextFormField(onChanged: (value) => password = value,
            obscureText: true,
            decoration: InputDecoration(hintText: AppLocalizations.of(context).translate("password")))
          ],),
        ),

        Row(children: [
        
        ElevatedButton(onPressed: () => {
         
          Navigator.pop(context)
        },
        style: ElevatedButton.styleFrom(primary: Colors.grey),
        child: Text(AppLocalizations.of(context).translate("cancelBtn")),
        ),

        ElevatedButton(onPressed: () async => {

          print(username),
          print(password),

          if(await checkUsernameAndPassword(username, password) == true && await checkIfAdmin(username) == 0) {

            Navigator.push(context, MaterialPageRoute(builder: (context) => UserMainScreen(userName: username)))
          }

          else if(await checkUsernameAndPassword(username, password) == true && await checkIfAdmin(username) == 1) {
            Navigator.push(context, MaterialPageRoute(builder: (context) => AdminMainScreen(userName: username,)))
          }

          else {

            ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text(AppLocalizations.of(context).translate("loginMsg")))),

          }

        },
        child: Text(AppLocalizations.of(context).translate("log_in")),
        style: ElevatedButton.styleFrom(primary: Colors.blue))

      ])

      
      
        ]));
  }

    checkUsernameAndPassword(String username, String password) async {

    List<User> users = await SQLiteDbProvider.db.getAllUsers();

    bool isCorrect;

    for(int i = 0; i < users.length; i++) {

      if(users[i].userName == username && users[i].password == password)

        isCorrect = true;


      }

      return isCorrect;

    }


   Future<int> checkIfAdmin(String username) async {

      List<User> users = await SQLiteDbProvider.db.getAllUsers();

      int isAdm = 0;


      for(int i = 0; i < users.length; i++) {

        if(users[i].userName == username && users[i].isAdmin == 1)

          isAdm = 1;

        

        }

        return isAdm;


      }

}





    



    
    




  


