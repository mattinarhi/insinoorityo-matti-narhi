import 'package:flutter/material.dart';
import 'package:inssityo/Database.dart';
import 'package:inssityo/appLocalization.dart';
import 'package:inssityo/user.dart';
import 'package:inssityo/validator.dart';

class ResetPasswordView extends StatefulWidget {

final  String username;

ResetPasswordView(this.username);
  @override
  _ResetPasswordViewState createState() => _ResetPasswordViewState();
}

class _ResetPasswordViewState extends State<ResetPasswordView> {

  final _formKey = GlobalKey<FormState>();

  Validator validator = Validator();

  String pwd;


  @override
  Widget build(BuildContext context) {
    return Scaffold(

      appBar: AppBar(title: Text(AppLocalizations.of(context).translate("reset_title") + widget.username),),

      body: SingleChildScrollView(child: Column(mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Center(child: FutureBuilder(

        future: SQLiteDbProvider.db.getUserByUserName(widget.username),

        builder: (context, AsyncSnapshot<User> snapshot) {
        
        if(snapshot.hasData) {
        
        
        
       return Form(autovalidateMode: AutovalidateMode.always,
          
          key: _formKey, 
        
          child: Column(
          
          children: [

            TextFormField(
              onChanged: (value) => snapshot.data.password = value,
              decoration: InputDecoration(hintText: AppLocalizations.of(context).translate("password")),
              validator: (value) => validator.passwordValidator(value, context),
            ),

            TextFormField(
              onChanged: (value) => pwd = value,
              decoration: InputDecoration(hintText: AppLocalizations.of(context).translate("password_check")),
              validator: (value) => validator.passwordMatchCheck(pwd, snapshot.data.password, context),
            ),

            Row(children: [
              ElevatedButton(onPressed: () {

                Navigator.pop(context);
              }, child: Text(AppLocalizations.of(context).translate("cancelBtn")),
              style: ElevatedButton.styleFrom(primary: Colors.grey)),

              ElevatedButton(onPressed: () async {

                if(_formKey.currentState.validate() == true) {

                  Navigator.pop(context);
                  await SQLiteDbProvider.db.updateUser(snapshot.data);
                  ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text(AppLocalizations.of(context).translate("reset_done") + widget.username)));
                }
                  else {

                    ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text(AppLocalizations.of(context).translate("check_input"))));
                  }
              }, child: Text(AppLocalizations.of(context).translate("resetBtn")),
              style: ElevatedButton.styleFrom(primary: Colors.green))
            ],)

        ]));
        
        }

        else {

          return CircularProgressIndicator();
        }
        
        })
      )],)),
      
    );
  }
}