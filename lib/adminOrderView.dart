import 'package:flutter/material.dart';
import 'package:inssityo/Database.dart';
import 'package:inssityo/appLocalization.dart';
import 'package:inssityo/order.dart';
import 'package:inssityo/orderDetailScreen.dart';
import 'package:inssityo/user.dart';

class AdminOrderView extends StatefulWidget {

  @override
  _AdminOrderViewState createState() => _AdminOrderViewState();
}

class _AdminOrderViewState extends State<AdminOrderView> {

  @override
  void initState() {

    super.initState();
  }


  @override
  Widget build(BuildContext context) {
       return Scaffold(

      appBar: AppBar(title: Text(AppLocalizations.of(context).translate("orders")),
      actions: [IconButton(onPressed: () {
        
        setState(() {
          
        });
      }, icon: Icon(Icons.refresh))],
      automaticallyImplyLeading: false),

      body: FutureBuilder(

        future: Future.wait([SQLiteDbProvider.db.getAllOrders(), SQLiteDbProvider.db.getAllUsers()]),

        builder: (context, AsyncSnapshot<List<dynamic>>snapshot) {

          if(snapshot.hasData) {

            List<Order> orders = snapshot.data[0];

            return ListView.builder(

              itemCount: orders.length,
              itemBuilder: (context, index) {

                return Card(

                  child: InkWell(

                    onTap: () {

                      Navigator.push(context, MaterialPageRoute(builder: (context) => OrderDetailScreen(orders[index])));
                    },

                    child: Column(

                      children:[

                          Text(AppLocalizations.of(context).translate("order_number") + ": " + orders[index].id.toString(), style: TextStyle(fontSize: 20.0),),


                          userText(orders[index].userId, snapshot.data[1]),

                          Text(AppLocalizations.of(context).translate("order_date") + ": " + orders[index].dateTime, style: TextStyle(fontSize: 20.0)),

                          Text(AppLocalizations.of(context).translate("total_price") + ": " + orders[index].price.toStringAsFixed(2) + " €", style: TextStyle(fontSize: 20.0),),

                          Text(AppLocalizations.of(context).translate("state") + ": " + orders[index].state, style: TextStyle(fontSize: 20.0),),

                          Text(AppLocalizations.of(context).translate("delivery_method") + ": " + orders[index].deliveryMethod, style: TextStyle(fontSize: 20.0),),

                          Row(children: [

                            orders[index].state == "Open" ? ElevatedButton(onPressed: () {

                              showCancelDialog(context, orders[index]);

                            }, child: Text(AppLocalizations.of(context).translate("cancelBtn")),
                            style: ElevatedButton.styleFrom(primary: Colors.red),)

                            : Text(""),

                            orders[index].state == "Open" ? ElevatedButton(onPressed: () {

                              showConfirmDialog(context, orders[index]);


                            }, child: Text(AppLocalizations.of(context).translate("confirmBtn")),
                            style: ElevatedButton.styleFrom(primary: Colors.blue),)

                            : Text("")




                          ],)






                      ]



                    ),



                  ),



                );





              },




            );



          }

          else {

           return CircularProgressIndicator();
          }
        },



      )


       );
      
    
  }

  Widget userText(int id, List<User> users) {


    final index = users.indexWhere((element) => element.id == id);




    


    


    return Text(AppLocalizations.of(context).translate("orderer") + ": " + users[index].userName, style: TextStyle(fontSize: 20.0),);



  }


  showCancelDialog(BuildContext context, Order order) {


    AlertDialog alertDialog = AlertDialog(

      title: Text(AppLocalizations.of(context).translate("warning")),

      content: Text(AppLocalizations.of(context).translate("cancel_order")),

      actions: [

        TextButton(onPressed: () {

          Navigator.of(context).pop();
        }, child: Text(AppLocalizations.of(context).translate("cancelBtn")),),

        TextButton(onPressed: () async {

          Navigator.of(context).pop();

          order.state = "Cancelled";
          
          
          await SQLiteDbProvider.db.updateOrder(order);

          setState(() {
            
          });
        }, child: Text(AppLocalizations.of(context).translate("yesBtn")),)
      ],
    );

    showDialog(context: context, 
    
    
    builder: (BuildContext context) {

      return alertDialog;
    });

  }



    showConfirmDialog(BuildContext context, Order order) {


    AlertDialog alertDialog = AlertDialog(

      title: Text(AppLocalizations.of(context).translate("warning")),

      content: Text(AppLocalizations.of(context).translate("confirm_order")),

      actions: [

        TextButton(onPressed: () {

          Navigator.of(context).pop();
        }, child: Text(AppLocalizations.of(context).translate("cancelBtn")),),

        TextButton(onPressed: () async {

          Navigator.of(context).pop();

          order.state = "Confirmed";
          
          
          await SQLiteDbProvider.db.updateOrder(order);

          setState(() {
            
          });
        }, child: Text(AppLocalizations.of(context).translate("yesBtn")),)
      ],
    );

    showDialog(context: context, 
    
    
    builder: (BuildContext context) {

      return alertDialog;
    });





  }





  }

  




