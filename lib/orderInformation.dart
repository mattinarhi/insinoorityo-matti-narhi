import 'package:inssityo/order.dart';
import 'package:inssityo/orderRow.dart';
import 'package:inssityo/product.dart';
import 'package:inssityo/user.dart';

class OrderInformation {


  List<Order> order;

  List<OrderRow> orderRow;

  List<Product> product;

  OrderInformation(this.order, this.orderRow, this.product);


  factory OrderInformation.fromMap(Map<String, dynamic> data) {

    return OrderInformation(

      data['order'],
      data['orderRow'],
      data['product']


    );
  }

  Map<String, dynamic> toMap() => {


    "order:": order,

    "orderRow": orderRow,

    "product": product
  };


  @override
  String toString() {

    return order.toString() + orderRow.toString() + product.toString();
  }


}