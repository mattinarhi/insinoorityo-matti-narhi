import 'dart:typed_data';

class Picture {

  final int id;

  final String title;

  final Uint8List picture;

  Picture(this.id, this.title, this.picture);



  factory Picture.fromMap(Map<String, dynamic> data) {

    return Picture(
      data['id'],
      data['title'],
      data['picture']
    );



  }


  Map<String, dynamic> toMap() => {

    "id": id,
    "title": title,
    "picture": picture
  };

  
}