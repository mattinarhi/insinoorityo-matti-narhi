import 'dart:async';

import 'package:flutter/material.dart';
import 'package:inssityo/Database.dart';
import 'package:inssityo/appLocalization.dart';
import 'package:inssityo/changePasswordView.dart';
import 'package:inssityo/updateProfileView.dart';
import 'package:inssityo/user.dart';

class Profile extends StatefulWidget {

  final String userName;

  Profile({this.userName});

  @override
  _ProfileState createState() => _ProfileState();
}


class _ProfileState extends State<Profile> {

  User user;

  String userName;

  @override
  void initState() {

    super.initState();
   
    print(widget.userName);


  }
   


  User getUser(String userName) {

    getUserInfo(userName);

    return user;


  }

  

  Future<void> getUserInfo(String userName) async {

    user = await SQLiteDbProvider.db.getUserByUserName(userName);


  }


  @override
  Widget build(BuildContext context) {

    
    return Scaffold(

      appBar: AppBar(title: Text(AppLocalizations.of(context).translate("profile")),
      automaticallyImplyLeading: false,),

      resizeToAvoidBottomInset: false,

      

      body: SingleChildScrollView(child: FutureBuilder(

        future: SQLiteDbProvider.db.getUserByUserName(widget.userName),
        builder: (context, AsyncSnapshot<User> snapshot ) {

          if(snapshot.hasData) {

            return Container(height: MediaQuery.of(context).size.height - 250, child: ListView.builder(

              scrollDirection: Axis.horizontal,

              itemCount: 1,
              
              itemBuilder: (context,index) {

            
          
            return Column(
            children: [Padding(padding: EdgeInsets.only(top: 30.0,),
            child: Text(AppLocalizations.of(context).translate("hello") + snapshot.data.firstName +"!", style: TextStyle(fontWeight: FontWeight.bold, fontSize: 25.0),),),

            Padding(

            padding: EdgeInsets.all(25.0),

            child: 

            Text(AppLocalizations.of(context).translate("profile_information"), style: TextStyle(fontSize: 25.0, decoration: TextDecoration.underline))),

          
            Card(
              color: Colors.blue,
              child: InkWell(
                onTap: () {

                  Navigator.push(context, MaterialPageRoute(builder: (context) => UpdateProfileView(widget.userName))).then(onGoBack);

                },
                child:  Column(children: [
                 Row(children:[Text(AppLocalizations.of(context).translate("username") + ": ", style: styleText(25.0, FontWeight.normal),), Text(snapshot.data.userName, style: styleText(
                    25.0, FontWeight.normal),)]
                ),
                Row(children: [FittedBox(fit: BoxFit.scaleDown, child: Text(AppLocalizations.of(context).translate("name") + ": ", style: styleText(25.0, FontWeight.normal),)), Text(snapshot.data.firstName, style: styleText(25.0, FontWeight.normal)), Text(" "), Text(snapshot.data.lastName, style: styleText(25.0, FontWeight.normal))]),

               Row(children: [Text(AppLocalizations.of(context).translate("address") + ": ", style: styleText(25.0, FontWeight.normal)), Text(snapshot.data.address, style: styleText(25.0, FontWeight.normal))]),

               Row(children: [Text(AppLocalizations.of(context).translate("postal_city") + ": ",style: styleText(25.0, FontWeight.normal)), Text(snapshot.data.postalCode, style: styleText(25.0, FontWeight.normal)), Text(" "), Text(snapshot.data.city, style: styleText(25.0, FontWeight.normal))]),

               Row(children: [Text(AppLocalizations.of(context).translate("phone_number") + ": ", style: styleText(25.0, FontWeight.normal), maxLines: 3,), Text(snapshot.data.phoneNumber, style: styleText(25.0, FontWeight.normal), maxLines: 3,)]),

              Row(children: [Text(AppLocalizations.of(context).translate("email") + ": ", style: styleText(25.0, FontWeight.normal), maxLines: 3,),Text(snapshot.data.email, style: styleText(25.0, FontWeight.normal),maxLines: 3,)]),

               Row(children: [Text(AppLocalizations.of(context).translate("dob") + ": ", style: styleText(25.0, FontWeight.normal)), Text(snapshot.data.dob, style: styleText(25.0, FontWeight.normal))])
                
               ]

                ),
              ),
          ),

            ElevatedButton(onPressed: () {

              Navigator.push(context, MaterialPageRoute(builder: (context) => ChangePasswordView(widget.userName)));

            }, child: Text(AppLocalizations.of(context).translate("change_password")), style:  ElevatedButton.styleFrom(primary: Colors.green),) 
            ],);}));

          }

          else {

            return Center(child: CircularProgressIndicator());
          }


          }

      
        
        
      
    )));
  }

  TextStyle styleText(double fontsize, FontWeight fontWeight) {

    return TextStyle(fontSize: fontsize, fontWeight: fontWeight, color: Colors.white);
  }

  FutureOr onGoBack(dynamic value) {

    setState(() {
      
    });



  }

}