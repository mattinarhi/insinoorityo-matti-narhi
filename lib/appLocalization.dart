import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class AppLocalizations {


  final Locale locale;

  AppLocalizations(this.locale);


  static AppLocalizations of(BuildContext context) {

    return Localizations.of<AppLocalizations>(context, AppLocalizations);
  }

  static const LocalizationsDelegate<AppLocalizations> delegate = _AppLocalizationsDelegate();

  Map<String, String> localizedStrings;


  Future<bool> load() async {


    String jsonString = await rootBundle.loadString("lib/i18n/${locale.languageCode}.json");

    Map<String, dynamic> jsonMap = jsonDecode(jsonString);

    localizedStrings = jsonMap.map((key, value) {

      return MapEntry(key, value);
    });

    return true;
  }

  String translate(String key) {

    return localizedStrings[key];
  }

  
}

class _AppLocalizationsDelegate extends LocalizationsDelegate<AppLocalizations> {


  const _AppLocalizationsDelegate();

  @override
  bool isSupported(Locale locale) {

    return ['en', 'fi'].contains(locale.languageCode);


  }

  @override
  Future<AppLocalizations> load(Locale locale) async {

    AppLocalizations localizations = AppLocalizations(locale);

    await localizations.load();

    return localizations;


  }

  @override
  bool shouldReload(_AppLocalizationsDelegate old) => false;


}