import 'dart:ffi';

import 'package:flutter/material.dart';
import 'package:inssityo/Database.dart';
import 'package:inssityo/appLocalization.dart';
import 'package:inssityo/order.dart';
import 'package:inssityo/orderDetailScreen.dart';

class UserOrderView extends StatefulWidget {

  final String username;

  UserOrderView(this.username);

  @override
  _UserOrderViewState createState() => _UserOrderViewState();
}

class _UserOrderViewState extends State<UserOrderView> {

  @override
  void initState() {

    super.initState();
  }

 



  @override
  Widget build(BuildContext context) {
       return Scaffold(

      appBar: AppBar(title: Text(AppLocalizations.of(context).translate("orders")),
      actions: [IconButton(onPressed: () {

        setState(() {
          
        });

      
      }, icon: Icon(Icons.refresh))],
      automaticallyImplyLeading: false,),

      body: FutureBuilder(

        future: SQLiteDbProvider.db.getUserOrders(widget.username),
        builder: (context, AsyncSnapshot<List<Order>> snapshot) {

          if(snapshot.hasData) {

            List<Order> orders = snapshot.data;

            return ListView.builder(

              itemCount: orders.length,

              itemBuilder: (context, index) {

                return Card(

                  child: InkWell(

                    onTap: () {

                      Navigator.push(context, MaterialPageRoute(builder: (context) => OrderDetailScreen(orders[index])));


                    },

                    child: Column(children: [

                      Text(AppLocalizations.of(context).translate("order_number") + ": " + orders[index].id.toString()),

                      Text(AppLocalizations.of(context).translate("order_date") + ": " + orders[index].dateTime),

                      Text(AppLocalizations.of(context).translate("total_price") + ": " + orders[index].price.toStringAsFixed(2) + " €"),

                      Text(AppLocalizations.of(context).translate("state") + ": " + orders[index].state),

                      Text(AppLocalizations.of(context).translate("delivery_method") + ": " + orders[index].deliveryMethod),

                      orders[index].state == "Open" ? ElevatedButton(onPressed: () {

                        showCancelDialog(context, orders[index]);


                      }, child: Text(AppLocalizations.of(context).translate("cancelBtn")),
                      
                      style: ElevatedButton.styleFrom(primary: Colors.red),)

                      :

                      Text("")



                    ],)


                  ),
                );




              },



            );

            


          }

          else {

            return CircularProgressIndicator();
          }

          
        },
      ),

      

        

      
      
      );
  }

  showCancelDialog(BuildContext context, Order order) {


    AlertDialog alertDialog = AlertDialog(

      title: Text(AppLocalizations.of(context).translate("warning")),

      content: Text(AppLocalizations.of(context).translate("cancel_order")),

      actions: [

        TextButton(onPressed: () {

          Navigator.of(context).pop();
        }, child: Text(AppLocalizations.of(context).translate("cancelBtn")),),

        TextButton(onPressed: () async {

          Navigator.of(context).pop();

          order.state = "Cancelled";
          
          
          await SQLiteDbProvider.db.updateOrder(order);

          setState(() {
            
          });
        }, child: Text(AppLocalizations.of(context).translate("yesBtn")),)
      ],
    );

    showDialog(context: context, 
    
    
    builder: (BuildContext context) {

      return alertDialog;
    });





  }
}