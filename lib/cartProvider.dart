import 'package:flutter/material.dart';
import 'package:inssityo/appLocalization.dart';
import 'package:inssityo/product.dart';

class CartProvider extends ChangeNotifier {


  List<Product> cart = [];


  int size = 0;

  double totalPrice = 0;

  String emptyCartMsg = "";


  void addProduct(Product product) {

    if(product.itemCount == 0) {


    cart.add(product);

    product.itemCount++;

    size++;

    addToTotalPrice(product);

    notifyListeners();

    }
    else if(product.itemCount >= 1){

  
          print("Product amount increased in shopping cart");

          product.itemCount++;
          size++;
          addToTotalPrice(product);
          notifyListeners();
        }
        else {

          cart.add(product);
          size++;

          addToTotalPrice(product);

          notifyListeners();
        }
      


    

    
  }

  void emptyCart(BuildContext context) {

    if(size > 0) {

      for(int i = 0; i < cart.length; i++) {

        cart[i].itemCount = 0;
      }



      cart.clear();
      emptyCartMsg = AppLocalizations.of(context).translate("emptyCartMsg");
      size = 0;
      totalPrice = 0;
      notifyListeners();
    }

    else {

      emptyCartMsg = AppLocalizations.of(context).translate("empty_already");

      notifyListeners();


    }
  }

  void addToTotalPrice(Product product) {


    totalPrice = totalPrice + product.price;

    notifyListeners();

  }

  void removeFromPrice(Product product) {

    totalPrice = totalPrice - product.price;

    notifyListeners();
  }

  void deleteProduct(Product product) {

    if(product.itemCount == 1) {

      cart.remove(product);
      product.itemCount--;
      size--;
      removeFromPrice(product);
      notifyListeners();
    }

    else {

      product.itemCount--;
      size--;
      removeFromPrice(product);
      notifyListeners();
    }
  }

  void adminProductDelete(Product product) {

    totalPrice = totalPrice - product.price*product.itemCount;

    product.itemCount = 0;

    cart.remove(product);

    notifyListeners();
  }


  


  List<Product> get shoppingcart => cart;

  int get cartSize => size;

  String get cartMsg => emptyCartMsg;

  double get cartPrice => totalPrice;













}