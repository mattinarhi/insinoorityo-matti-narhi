import 'package:flutter/material.dart';
import 'package:inssityo/Database.dart';
import 'package:inssityo/appLocalization.dart';
import 'package:inssityo/user.dart';
import 'package:inssityo/validator.dart';

class ChangePasswordView extends StatefulWidget {

  final String username;

  ChangePasswordView(this.username);


  @override
  _ChangePasswordViewState createState() => _ChangePasswordViewState();
}

class _ChangePasswordViewState extends State<ChangePasswordView> {

  Validator validator = Validator();

  final _formKey = GlobalKey<FormState>();

  String pwd;

  String oldPwd;

  String newPwd1;

  String newPwd2;


  @override
  Widget build(BuildContext context) {
    return Scaffold(
    
    resizeToAvoidBottomInset: false,

    appBar: AppBar(title: Text(AppLocalizations.of(context).translate("change_password"))),

    body:

    SingleChildScrollView(child: 
    
    FutureBuilder(

      future: SQLiteDbProvider.db.getUserByUserName(widget.username),
      builder: (context, AsyncSnapshot<User> snapshot) {

        if(snapshot.hasData) {

          pwd = snapshot.data.password;

          return Column(children: [

            Form(key: _formKey,
            autovalidateMode: AutovalidateMode.always,
            child: Column(mainAxisAlignment: MainAxisAlignment.center,
              
              children: [

                TextFormField(onChanged: (value) => oldPwd = value,
                
                validator: (value) => validator.checkPassword(value, pwd, context),
                decoration: InputDecoration(hintText: AppLocalizations.of(context).translate("old_password"))),

                TextFormField(
                  onChanged: (value) => snapshot.data.password = value,
                  validator: (value) => validator.passwordValidator(value, context),
                  decoration: InputDecoration(hintText: AppLocalizations.of(context).translate("new_password")),
                ),

                TextFormField(
                  onChanged: (value) => newPwd2 = value,
                  validator: (value) => validator.passwordMatchCheck(value, snapshot.data.password, context),
                  decoration: InputDecoration(hintText: AppLocalizations.of(context).translate("password_check")),
                ),

                Row(children: [

                  ElevatedButton(onPressed: () {

                    Navigator.pop(context);
                  }, child: Text(AppLocalizations.of(context).translate("cancelBtn")),
                  style: ElevatedButton.styleFrom(primary: Colors.grey),),

                  ElevatedButton(onPressed: () async {

                    if(_formKey.currentState.validate() == true) {

                      await SQLiteDbProvider.db.updateUser(snapshot.data);
                      Navigator.pop(context);

                    }

                    else {

                      ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text(AppLocalizations.of(context).translate("check_input"))));
                    }


                  }, child: Text(AppLocalizations.of(context).translate("changeBtn")),
                  style: ElevatedButton.styleFrom(primary: Colors.blue))
                ],)



            ],))

          ],);
        }
          else {

            return Column(mainAxisAlignment: MainAxisAlignment.center,
            children: [Center(child: CircularProgressIndicator())]);
          }
        
      },

    )));
      
    
  }
}