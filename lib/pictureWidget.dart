import 'dart:io';
import 'dart:typed_data';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:image_picker/image_picker.dart';
import 'package:inssityo/appLocalization.dart';

class PictureWidget extends StatefulWidget {

  static bool imgAdded = false;

  static File img;

  PictureWidget();

  



  @override
  _PictureWidgetState createState() => _PictureWidgetState();
}

class _PictureWidgetState extends State<PictureWidget> {

  final ImagePicker imagePicker = ImagePicker();

  File _image;


  ByteData bytes;

  Uint8List tmpImage;

  @override
  void initState() {

    super.initState();



  }

  @override
  void dispose() {

    super.dispose();

    PictureWidget.img = null;

  }


  Future<void> fetchAssetImage() async {

    bytes = await rootBundle.load("lib/assets/images/flutter.png");

    tmpImage = bytes.buffer.asUint8List();





  }







  @override
  Widget build(BuildContext context) {

    _image = PictureWidget.img;


    return Container(

      width: MediaQuery.of(context).size.width,

      height: 200,

      child: Column(
      children: [
        SizedBox(
          height: 32,
        ),
        Center(
          child: GestureDetector(
            onTap: () {
              showPicker(context);
            },
            child: Container(
              
              child: _image != null
                  ? Container(
                      child: Image.file(
                        _image,
                        width: 100,
                        height: 100,
                        fit: BoxFit.fitHeight,
                      ),
                    )
                  : Container(
                      decoration: BoxDecoration(
                          color: Colors.grey[200],
                          ),
                      width: 100,
                      height: 100,
                      child: Icon(Icons.camera_alt),
                      ),
                    ),
            ),
          ),
        
      ],
    )



      
    );
  }

  _imgFromCamera() async {

    XFile image = await imagePicker.pickImage(source: ImageSource.camera, imageQuality: 50);

    setState(() {

      if(image == null) {

        return;
      }
      else {
      
      _image = File(image.path);
      PictureWidget.img = _image;
      PictureWidget.imgAdded = true;
      }
    });



  }

  _imgFromGallery() async {

    XFile image = await imagePicker.pickImage(source: ImageSource.gallery, imageQuality: 50);

    setState(() {

       if(image == null) {

        return;
      }
      else {
      
      
      _image = File(image.path);
      PictureWidget.img = _image;
      PictureWidget.imgAdded = true;

      }
    });
  }

  void showPicker(BuildContext context) {

      showModalBottomSheet(
      context: context,
      builder: (BuildContext bc) {
        return SafeArea(
          child: Container(
            child: new Wrap(
              children: <Widget>[
                new ListTile(
                    leading: new Icon(Icons.photo_library),
                    title: new Text(AppLocalizations.of(context).translate("photo_library")),
                    onTap: () {
                      _imgFromGallery();
                      Navigator.of(context).pop();
                    }),
                new ListTile(
                  leading: new Icon(Icons.photo_camera),
                  title: new Text(AppLocalizations.of(context).translate("camera")),
                  onTap: () {
                    _imgFromCamera();
                    Navigator.of(context).pop();
                  },
                ),
              ],
            ),
          ),
        );
      }
    );


  }
}