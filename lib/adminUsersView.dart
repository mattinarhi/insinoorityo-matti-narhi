import 'dart:async';
import 'package:flutter/material.dart';
import 'package:inssityo/Database.dart';
import 'package:inssityo/adminUpdateProfile.dart';
import 'package:inssityo/appLocalization.dart';
import 'package:inssityo/resetPasswordView.dart';
import 'package:inssityo/user.dart';

class AdminUsersView extends StatefulWidget {

  @override
  _AdminUsersViewState createState() => _AdminUsersViewState();
}

class _AdminUsersViewState extends State<AdminUsersView> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text(AppLocalizations.of(context).translate("user_profiles")),
      automaticallyImplyLeading: false),
      body:  FutureBuilder(

        future: SQLiteDbProvider.db.getAllUsers(),
        builder: (context, AsyncSnapshot<List<User>> snapshot) {

          if(snapshot.hasData) {

            List<User> users = snapshot.data.toList();

            return ListView.builder(
         
            itemCount: users.length,
            itemBuilder: (context, index) {

              return Card(child: InkWell(
                onTap: () {
                 Navigator.push(context, MaterialPageRoute(builder: (context) => AdminUpdateProfile(snapshot.data[index].userName))).then(onGoBack);
                },

                child: Column(children: [Text(snapshot.data[index].userName),
                
                Text(""),

                Text(""),


                Text(snapshot.data[index].email),
                Text(snapshot.data[index].phoneNumber),
                Text(""),

                Text(snapshot.data[index].isAdmin == 1 ? AppLocalizations.of(context).translate("admin_yes") : AppLocalizations.of(context).translate("admin_no")),

                Row(children: [

                  ElevatedButton(onPressed: () async{

                    if(snapshot.data[index].userName == "admin") {

                      ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text(AppLocalizations.of(context).translate("cannot_delete"))));
                    }

                    else {


                    showAlertDialog(context, snapshot.data[index].id, snapshot.data[index].userName);

                    }


                  }, child: Icon(Icons.delete), style: ElevatedButton.styleFrom(primary: Colors.blue)),


                  ElevatedButton(onPressed:() {

                    Navigator.push(context, MaterialPageRoute(builder: (context) => ResetPasswordView(snapshot.data[index].userName)));


                  }, child: Text(AppLocalizations.of(context).translate("reset_password")),
                  style: ElevatedButton.styleFrom(primary: Colors.green),)






                ],)

                
                ],
              )));
            }
            );}
          else {
            return Column(mainAxisAlignment: MainAxisAlignment.center,
            children: [Center(child: CircularProgressIndicator(),)],);
          }
        },


      )
    );
      
    
  }

  FutureOr onGoBack(dynamic value) {

    setState(() {
      
    });
  }

  showAlertDialog(BuildContext context, int id, String user) {


    AlertDialog alertDialog = AlertDialog(

      title: Text(AppLocalizations.of(context).translate("warning")),

      content: Text(AppLocalizations.of(context).translate("delete_user") + user + " ?"),

      actions: [
        TextButton(child: Text(AppLocalizations.of(context).translate("cancelBtn")),
        onPressed: () {

          Navigator.of(context).pop();
        }),

        TextButton(child: Text(AppLocalizations.of(context).translate("yesBtn")),
        onPressed: () async {

          Navigator.of(context).pop();
          await SQLiteDbProvider.db.deleteUser(id);
          ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text(AppLocalizations.of(context).translate("user") + user + AppLocalizations.of(context).translate("deleted"))));
          setState(() {
            
          });
        }
        )],
    );

    showDialog(context: context, 
    builder: (BuildContext context) {

      return alertDialog;
    }
    );}
}