import 'dart:async';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:inssityo/addProductView.dart';
import 'package:inssityo/appLocalization.dart';
import 'package:inssityo/product.dart';
import 'package:inssityo/updateProduct.dart';
import 'package:path_provider/path_provider.dart';
import 'package:provider/provider.dart';

import 'Database.dart';
import 'cartProvider.dart';

class ProductAdminView extends StatefulWidget {

  @override
  _ProductAdminViewState createState() => _ProductAdminViewState();
}

class _ProductAdminViewState extends State<ProductAdminView> {

  List<Product> duplicateItems = [];

  List<Product> items = [];


    @override
  void initState() {

    super.initState();

    fetchProductData().whenComplete(() {

      items.addAll(duplicateItems);
    });

     

    items.addAll(duplicateItems);



  }

  Future<void> fetchProductData() async{

  duplicateItems = await SQLiteDbProvider.db.getAllProducts();

    

  }

  void filterSearchResults(String query) {

    List<Product> dummySearchList = [];

    dummySearchList.addAll(duplicateItems);

    if(query.isNotEmpty) {

      List<Product> dummyListData = [];

      dummySearchList.forEach((element) {

        if(element.name.contains(query)) {

          dummyListData.add(element);
        }

      });

      setState(() {
        items.clear();
        items.addAll(dummyListData);
      });
    }

    else {

      setState(() {
        
        items.clear();
        items.addAll(duplicateItems);
      });
    }


  }









  @override
  Widget build(BuildContext context) {
     
return
    

       Scaffold(

      appBar: AppBar(title: Text(AppLocalizations.of(context).translate("products")),
      automaticallyImplyLeading: false,
      actions: [TextButton(child: Text(AppLocalizations.of(context).translate("add_new"), style: TextStyle(color: Colors.white),),
      onPressed: () {

        Navigator.push(context, MaterialPageRoute(builder: (context) => AddProductView(items))).then(onGoBack);


      },)],),

      body:  FutureBuilder(

        future: SQLiteDbProvider.db.getAllProducts(),
        builder: (context, AsyncSnapshot<List<Product>> snapshot) {

          if(snapshot.hasData) {

      
        
            
            return Consumer<CartProvider>(


          builder: (context, provider, child) {


            
            
          return Column(
            
            children: [
            
            TextField(
              onChanged: (value) {

                filterSearchResults(value);

                
                
              },
              decoration: InputDecoration(prefixIcon: Icon(Icons.search)),
            ),
          
          Expanded(child: 
           
           ListView.builder(
            
            scrollDirection: Axis.vertical,
              
            itemCount:items.length,

            itemBuilder: (context, index) {


              return Card(child: 
              InkWell(

                onTap: () async {

                    

                      Directory tmpDir = await getTemporaryDirectory();

                   
                   final img = await File('${tmpDir.path}/image{$index}.jpg').create();

                   await img.writeAsBytes(items[index].picture);

                  Navigator.push(context, MaterialPageRoute(builder: (context) => UpdateProduct(img, items[index], items))).then((onGoBack));

                },
                
                
                
                child: Column(children: [

                  Container(
                    height: 150,
                    width: MediaQuery.of(context).size.width,
                    child: Image.memory(items[index].picture),
                  ),

                  Text(items[index].name, style: TextStyle(fontSize: 20.0, fontWeight: FontWeight.bold)),

                  Text(AppLocalizations.of(context).translate("price") + ": " + items[index].price.toStringAsFixed(2) + " €", style: TextStyle(fontSize: 20.0, fontWeight: FontWeight.bold)),

                 Row(children: [

                  ElevatedButton(onPressed: () {

                    showAlertDialog(context, items[index], items);

           
       


                  }, child: Icon(Icons.delete),
                  
                  style: ElevatedButton.styleFrom(primary: Colors.blue),),
                   
                   
                   
                   
                   ElevatedButton(onPressed: () {

                    provider.addProduct(items[index]);

                    print(provider.cartSize);

                   





                  }, child: Text("+"),
                  style: ElevatedButton.styleFrom(primary: Colors.green),)


                ])],)));

                

            
          }))]);});
          }

          else {

            return CircularProgressIndicator();
          }
        },

   
      ));
  }

  showAlertDialog(BuildContext context, Product product, List<Product> products) {


    AlertDialog alertDialog = AlertDialog(

      title: Text(AppLocalizations.of(context).translate("warning")),

      content: Text(AppLocalizations.of(context).translate("delete_product") + product.name + " ?"),

      actions: [
        TextButton(onPressed: () {
          Navigator.of(context).pop();
        }, child: Text(AppLocalizations.of(context).translate("cancelBtn")),),

        TextButton(onPressed: () async {


          if(Provider.of<CartProvider>(context, listen: false).shoppingcart.contains(product)) {


            Provider.of<CartProvider>(context, listen: false).adminProductDelete(product);
          }

          

          await SQLiteDbProvider.db.deleteProduct(product.id);

          Navigator.of(context).pop();

          ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text(AppLocalizations.of(context).translate("product") + " " + product.name + " " + AppLocalizations.of(context).translate("deleted"))));

                   setState(() {




                     duplicateItems.remove(product);
                      
                     items.clear();

                     items.addAll(duplicateItems);
                    });


         


        },child: Text(AppLocalizations.of(context).translate("yesBtn")))
      ],
    );

    showDialog(context: context, 
    
    builder: (BuildContext context) {

      return alertDialog;
    });
  }

   FutureOr onGoBack(dynamic value) async {

     List<Product> products = await SQLiteDbProvider.db.getAllProducts();

    setState(() {

      items.clear();

      items.addAll(products);

      duplicateItems.clear();

      duplicateItems.addAll(items);

      
    });
   }

   afterUpdate() {

     setState(() {
       
     });
   }
}