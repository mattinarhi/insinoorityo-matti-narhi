import 'package:flutter/cupertino.dart';
import 'package:shared_preferences/shared_preferences.dart';

class LanguageProvider extends ChangeNotifier {

  Locale appLocale = Locale('en');

  Locale get appLocal => appLocale ?? Locale('en');

  fetchLocale() async {

    var prefs = await SharedPreferences.getInstance();

    if(prefs.getString('language_code') == null) {

      appLocale = Locale('en');
      return Null;
    }

    else {

      
      appLocale = Locale(prefs.getString('language_code'));

      return Null;
    }


  }

  void changeLanguage(Locale type) async{

    var prefs = await SharedPreferences.getInstance();

    if(appLocale == type) {

      return;
    }

    if(type == Locale("fi")) {

      appLocale = Locale("fi");

      await prefs.setString('language_code', 'fi');

      await prefs.setString('countryCode', '');
    }

    else {

      appLocale = Locale("en");

      await prefs.setString('language_code', 'en');

      await prefs.setString('countryCode', 'US');
    }

    notifyListeners();


  }



}