
import 'package:flutter/material.dart';
import 'package:inssityo/appLocalization.dart';
import 'package:inssityo/cartProvider.dart';
import 'package:inssityo/productUserView.dart';
import 'package:inssityo/profile.dart';
import 'package:inssityo/settings.dart';
import 'package:inssityo/shoppingCart.dart';
import 'package:inssityo/userOrderView.dart';
import 'package:provider/provider.dart';

class UserMainScreen extends StatefulWidget {

  final String userName;

  UserMainScreen({this.userName});

  @override
  _UserMainScreenState createState() => _UserMainScreenState();
}

class _UserMainScreenState extends State<UserMainScreen> {

  String username;

  List<Widget> _children;


  int _currentIndex = 0;

  @override
  void initState() {


    super.initState();

    _children = [Profile(userName: widget.userName), UserOrderView(widget.userName), ProductUserView(), ShoppingCart(widget.userName)];
    print(widget.userName);



  }


  @override
  Widget build(BuildContext context) {

    return ChangeNotifierProvider<CartProvider>(

      create: (context) => CartProvider(),

      child: Builder(

      builder: (context) {


    return WillPopScope(onWillPop: () async => false,
      
      child: Scaffold(

        appBar: AppBar(actions: [IconButton(onPressed: () {
          Navigator.push(context, MaterialPageRoute(builder: (context) => Settings()));
        }, icon: Icon(Icons.settings)),
        IconButton(onPressed: () {

          showAlertDialog(context);
        }, icon: Icon(Icons.logout))],
        automaticallyImplyLeading: false,),

        body: IndexedStack(
          index: _currentIndex,
          children: _children,
        ),

        bottomNavigationBar: BottomNavigationBar(

          currentIndex: _currentIndex,

          unselectedItemColor: Colors.grey,

          selectedItemColor: Colors.blue,

          onTap: (index) => setState(() => _currentIndex = index),


        items: [
          BottomNavigationBarItem(
            icon: Icon(Icons.person),
            label: AppLocalizations.of(context).translate("profile")
          ),

          BottomNavigationBarItem(

            icon: Icon(Icons.list),
            label: AppLocalizations.of(context).translate("orders")

          ),

          BottomNavigationBarItem(

            icon: Icon(Icons.local_mall),
            label: AppLocalizations.of(context).translate("products")
          ),

          BottomNavigationBarItem(

            icon: Icon(Icons.shopping_cart),
            label: AppLocalizations.of(context).translate("shopping_cart")
          ),

         
        ],
          

        )



    ));}));
      
    
  }

    showAlertDialog(BuildContext context) {

    AlertDialog alertDialog = AlertDialog(

      title: Text(AppLocalizations.of(context).translate("warning")),
      content: Text(AppLocalizations.of(context).translate("log_out")),
      actions: [
        TextButton(child: Text(AppLocalizations.of(context).translate("cancelBtn")),
        onPressed:() {

          Navigator.of(context).pop();
        }),

        TextButton(child: Text(AppLocalizations.of(context).translate("yesBtn")),
        onPressed: () {

          Navigator.of(context).pop();
          Navigator.of(context).popUntil((route) => route.isFirst);
        })
      ],



    );

    showDialog(context: context, 
    builder: (BuildContext context) {
      return alertDialog;
    }
  );
    }




  
}