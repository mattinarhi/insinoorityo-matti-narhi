import 'package:flutter/material.dart';
import 'package:inssityo/Database.dart';
import 'package:inssityo/appLocalization.dart';
import 'package:inssityo/user.dart';
import 'package:inssityo/validator.dart';

class UpdateProfileView extends StatefulWidget {

  final String username;

  UpdateProfileView(this.username);

  @override
  _UpdateProfileViewState createState() => _UpdateProfileViewState();
}

class _UpdateProfileViewState extends State<UpdateProfileView> {

  Validator validator = Validator();

  final _formkey = GlobalKey<FormState>();




  @override
  Widget build(BuildContext context) {


      return Scaffold(

        appBar: AppBar(title: Text(AppLocalizations.of(context).translate("update_profile"))),

        body: SingleChildScrollView(child: FutureBuilder(
          future: SQLiteDbProvider.db.getUserByUserName(widget.username),
          builder: (context, AsyncSnapshot<User> snapshot) {

            if(snapshot.hasData) {

              return Form(
                key: _formkey,
                autovalidateMode: AutovalidateMode.always,
                child:  Column(children:[ TextFormField(initialValue: snapshot.data.firstName,
                cursorHeight: 5.0,
                validator:(value) => validator.nameValidator(value, context),
                decoration: InputDecoration(labelText: AppLocalizations.of(context).translate("first_name")),
                onChanged: (value) => snapshot.data.firstName = value,),

                TextFormField(

                initialValue: snapshot.data.lastName,
                validator:(value) => validator.nameValidator(value, context),
                decoration: InputDecoration(labelText: AppLocalizations.of(context).translate("last_name")),
                onChanged: (value) => snapshot.data.lastName = value,


                ),

                TextFormField(

                  initialValue: snapshot.data.address,
                  validator: (value) => validator.addressValidator(value, context),
                  decoration: InputDecoration(labelText: AppLocalizations.of(context).translate("address")),
                  onChanged: (value) => snapshot.data.address = value,

                ),

                TextFormField(
                  initialValue: snapshot.data.postalCode,
                  validator: (value) => validator.postalCodeValidator(value, context),
                  decoration: InputDecoration(labelText: AppLocalizations.of(context).translate("postal_code")),
                  onChanged: (value) => snapshot.data.postalCode = value,

                ),

                TextFormField(
                  initialValue: snapshot.data.city,
                  validator: (value) => validator.cityNameValidator(value, context),
                  decoration: InputDecoration(labelText: AppLocalizations.of(context).translate("city")),
                  onChanged: (value) => snapshot.data.city = value,

                ),

               TextFormField(

                  initialValue: snapshot.data.email,
                  validator: (value) => validator.validateEmail(value, context),
                  decoration: InputDecoration(labelText: AppLocalizations.of(context).translate("email")),
                  onChanged: (value) => snapshot.data.email = value,

                ),

                TextFormField(

                  initialValue: snapshot.data.phoneNumber,
                  validator: (value) => validator.phoneNumberValidator(value, context),
                  decoration: InputDecoration(labelText: AppLocalizations.of(context).translate("phone_number")),
                  onChanged: (value) => snapshot.data.phoneNumber = value,
                
                ),
               TextFormField(

                  initialValue: snapshot.data.dob,
                  validator: (value) => validator.dobValidator(value, context),
                  decoration: InputDecoration(labelText: AppLocalizations.of(context).translate("dob")),
                  onChanged: (value) => snapshot.data.dob = value,


                ),
         Row(children: [

                ElevatedButton(

                  style: ElevatedButton.styleFrom(primary: Colors.grey),
                  child: Text(AppLocalizations.of(context).translate("cancelBtn")),
                  onPressed: () {

                    Navigator.pop(context);
                  },


                ),

               
                

                ElevatedButton(child: Text(AppLocalizations.of(context).translate("updateBtn")),
                style: ElevatedButton.styleFrom(primary: Colors.blue),
                onPressed: () async {
                if(_formkey.currentState.validate() == true) {
                  print(snapshot.data.firstName);
                  print(snapshot.data.lastName);
                  SQLiteDbProvider.db.updateUser(snapshot.data);
                  Navigator.pop(context);
                  }
                  else {
                    ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text(AppLocalizations.of(context).translate("check_input"))));
                  }
                },)])]));
            }

            else {

              return Column(mainAxisAlignment: MainAxisAlignment.center,
              children: [Center(child: Text(AppLocalizations.of(context).translate("error_msg")),)],);
            }

            
          },
        ),    
    ));
  }
}