import 'package:flutter/material.dart';
import 'package:inssityo/appLocalization.dart';
import 'package:inssityo/product.dart';
import 'package:inssityo/user.dart';

class Validator {


Validator();


String result;


  userNameValidator(String value, List<User> users, BuildContext context) {


    checkIfExists(value, users, context);


    if(result != null) {

      return result;

    }

    else if(value.length < 3 || value.length > 25) {

      return AppLocalizations.of(context).translate("invalid_username");


    }

      else {

        return null;


      }





    }

    String productNameValidator(String value, List<Product> products, BuildContext context) {

      String res = "";

      for(int i = 0; i < products.length; i++) {

        if(value == products[i].name) {

          res = AppLocalizations.of(context).translate("product_exists");
          break;

        }
      }

      if(res != "") {

        return res;
      }

      else if(value.length < 3) {

        return AppLocalizations.of(context).translate("invalid_product");
      }

      else {

        return null;
      }



      

    }

    String descriptionValidator(String value, BuildContext context) {

      if(value.length < 5) {

        return AppLocalizations.of(context).translate("invalid_description");
      }

      else {

        return null;
      }



    }

    String priceValidator(String value, BuildContext context) {

      RegExp regExp = RegExp(r"^[0-9]*\.[0-9]{2}$");

      if(regExp.hasMatch(value)) {

        return null;
      }

      else {

        return AppLocalizations.of(context).translate("invalid_price");
      }
    }



    

    


    







checkIfExists(String value, List<User> users, BuildContext context) {





  result = null;




  for(int i = 0; i < users.length; i++) {

    if(users[i].userName == value) {

      result = AppLocalizations.of(context).translate("username_exists");
      break;


    }
  
  }


}

String dobValidator(String value, BuildContext context) {

  RegExp regExp = new RegExp(r"^\s*(3[01]|[12][0-9]|0?[1-9])\.(1[012]|0?[1-9])\.((?:19|20)\d{2})\s*$");

  if(regExp.hasMatch(value) == true) {

    return null;



  }

  else {

    return AppLocalizations.of(context).translate("invalid_dob");
  }




}

String passwordValidator(String value, BuildContext context) {

  if(value.length >= 8 && value.contains(new RegExp(r'[A-Z]'))) {

    return null;
  }

  else {


    return AppLocalizations.of(context).translate("invalid_password");
  }



}


String passwordMatchCheck(String value1, String value2, BuildContext context) {


  if(value1 == value2) {


    return null;
  }

  else {

    return AppLocalizations.of(context).translate("password_match");
  }



}

String validateEmail(String value, BuildContext context) {

  RegExp regExp = new RegExp(r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+");

  if(regExp.hasMatch(value) == true) {

    return null;
  
  }

    else {

      return AppLocalizations.of(context).translate("invalid_email");
    }



}

String nameValidator(String value, BuildContext context) {


  if(value.length < 2) {

    return AppLocalizations.of(context).translate("invalid_name");


  }

    else {

      return null;


    }




}


String postalCodeValidator(String value, BuildContext context) {

  RegExp regExp = new RegExp(r"\d\d\d\d\d");

  if(regExp.hasMatch(value) == true) {

    return null;


  }

    else {

      return AppLocalizations.of(context).translate("invalid_postalCode");
    }




}


String cityNameValidator(String value, BuildContext context) {


  if(value.length < 2) {

    return AppLocalizations.of(context).translate("invalid_city");
  }

  else {

    return null;
  }
}

String addressValidator(String value, BuildContext context) {


  if(value.length < 5) {


    return AppLocalizations.of(context).translate("invalid_address");


  }

  else {

    return null;
  }
}

String phoneNumberValidator(String value, BuildContext context) {

  RegExp regExp = new RegExp(r"\d\d\d\d\d\d\d\d\d\d");

  if(regExp.hasMatch(value) == true) {

    return null;
  }

    else {

      return AppLocalizations.of(context).translate("invalid_phoneNumber");
    }



}

String checkPassword(String value1, String value2, BuildContext context) {


  if(value1 == value2) {

    return null;
  }

  else {

    return AppLocalizations.of(context).translate("wrong_password");
  }
}

String adminValidation(String value, BuildContext context) {

  if(value.length < 2 && value.contains(RegExp(r"[0-1]"))) {

    return null;
  }

  else {

    return AppLocalizations.of(context).translate("admin_validation");
  }
}

 
  }
      

    
    
    
  

  


  







    

  

  
    


 




  



  







