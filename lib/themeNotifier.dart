import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class ThemeNotifier extends ChangeNotifier {

  final String key = "theme";

  SharedPreferences pref;

  bool darktheme;

  bool get darkTheme => darktheme;

  ThemeNotifier() {

    darktheme = false;

    loadFromPrefs();
  }


  ThemeData light = ThemeData(

    brightness: Brightness.light
  );

  ThemeData dark = ThemeData(

    brightness: Brightness.dark
  );


  toggleTheme() {

    darktheme = !darktheme;

    saveToPrefs();

    notifyListeners();
  }


  initPrefs() async {

    if(pref == null) {

      pref = await SharedPreferences.getInstance();
    }
  }

  loadFromPrefs() async {

    await initPrefs();

    darktheme = pref.getBool(key) ?? true;

    notifyListeners();
  }

  saveToPrefs() async {

    await initPrefs();

    pref.setBool(key, darktheme);



  }





  




}