class OrderRow {

  int id;

  int orderId;

  int productId;

  int itemCount;


  OrderRow(this.id, this.orderId, this.productId, this.itemCount);


  factory OrderRow.fromMap(Map<String, dynamic> data) {

    return OrderRow(

      data['id'],
      data['orderId'],
      data['productId'],
      data['itemCount']

    );
  }

  Map<String, dynamic> toMap() => {

    "id": id,

    "orderId": orderId,

    "productId": productId,

    "itemCount": itemCount


  };

  @override
  String toString() {

    return "Order row id: " + id.toString() + " Order id: " + orderId.toString() + " Product id: " + productId.toString() + " itemCount " + itemCount.toString();
  }



}