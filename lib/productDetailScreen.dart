import 'package:flutter/material.dart';
import 'package:inssityo/appLocalization.dart';
import 'package:inssityo/product.dart';

class ProductDetailScreen extends StatefulWidget {

  final Product product;

  ProductDetailScreen(this.product);

  @override
  _ProductDetailScreenState createState() => _ProductDetailScreenState();
}

class _ProductDetailScreenState extends State<ProductDetailScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(

      appBar: AppBar(title: Text("Product information"),),

      body: Column(


        
        
        children: [

        Padding(padding: EdgeInsets.only(top: 10.0),

        child: 

        Align(

        alignment: Alignment.topCenter,

        child: 

        Container(


          height: 200,

          width: 200,
          
          
          child:

       Image.memory(

         
         
         
         widget.product.picture))
        



      )),
      
    Padding(padding: EdgeInsets.only(top: 30.0), child:  Align(alignment: Alignment.centerLeft, child: Text(AppLocalizations.of(context).translate("product_name") + ": " + widget.product.name, style: TextStyle(fontSize: 20.0),))),

     Align(alignment: Alignment.centerLeft, child: Text(AppLocalizations.of(context).translate("product_description") + ": " + widget.product.description, style: TextStyle(fontSize: 20.0))),

    Align(alignment: Alignment.centerLeft, child: Text(AppLocalizations.of(context).translate("category") + ": " + widget.product.category, style: TextStyle(fontSize: 20.0))),

    Align(alignment: Alignment.centerLeft, child:  Text(AppLocalizations.of(context).translate("price") + ": " + widget.product.price.toStringAsFixed(2) + " €", style: TextStyle(fontSize: 20.0)))
      
      
      
      ],),
      
    );
  }
}