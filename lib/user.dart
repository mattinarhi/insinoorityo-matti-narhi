class User {

  int id;

  String userName;

  String password;

  String firstName;

  String lastName;

  String address;

  String postalCode;

  String city;

  String phoneNumber;

  String email;

  String dob;

  int isAdmin;

  static final columns = ["id", "userName", "password", "firstName", "lastName", "address", "postalCode", "city", "phoneNumber", "email", "dob", "isAdmin"];


  User(this.id, this.userName, this.password, this.firstName, this.lastName, this.address, this.postalCode, this.city, this.phoneNumber, this.email, this.dob, this.isAdmin);


  factory User.fromMap(Map<String, dynamic> data) {

    return User(
      data['id'],
      data['userName'],
      data['password'],
      data['firstName'],
      data['lastName'],
      data['address'],
      data['postalCode'],
      data['city'],
      data['phoneNumber'],
      data['email'],
      data['dob'],
      data['isAdmin']
    );
  }

  Map<String, dynamic> toMap() => {

    "id": id,
    "userName": userName,
    "password": password,
    "firstName": firstName,
    "lastName": lastName,
    "address": address,
    "postalCode": postalCode,
    "city": city,
    "phoneNumber": phoneNumber,
    "email": email,
    "dob": dob,
    "isAdmin": isAdmin




  };

  @override
  String toString() {

  return "{id: $id, username: $userName, password: $password, firstName: $firstName, lastName: $lastName, address: $address, postalCode: $postalCode, city: $city, phoneNumber: $phoneNumber, email: $email, dob: $dob, isAdmin: $isAdmin}";


  }




}