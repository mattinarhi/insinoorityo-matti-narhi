import 'package:inssityo/product.dart';

class Cart {


  static List<Product> cart = [];

  static int size = 0;



  static List<Product> getCart() {

    return cart;
  }

  static void addProduct(Product product) {

    cart.add(product);
    size++;
  }

  static int getCartSize() {

    return size;
  }

  static void deleteProduct(Product product) {

    cart.remove(product);
    size--;
  }



}