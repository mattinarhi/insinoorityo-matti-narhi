import 'dart:io';

import 'package:flutter/material.dart';
import 'package:inssityo/Database.dart';
import 'package:inssityo/appLocalization.dart';
import 'package:inssityo/pictureWidget.dart';
import 'package:inssityo/product.dart';
import 'package:inssityo/validator.dart';
import 'package:path_provider/path_provider.dart';

class UpdateProduct extends StatefulWidget {

  final File img;

  final Product product;

  final List<Product> products;

  UpdateProduct(this.img, this.product, this.products);



  @override
  _UpdateProductState createState() => _UpdateProductState();
}

class _UpdateProductState extends State<UpdateProduct> {

  PictureWidget pictureWidget;

  File img;

  Product product;
  
  final _formKey = GlobalKey<FormState>();

  Validator validator = Validator();

  List<String> categories = ["Practice", "Clothes", "Shoes", "Equipment", "Other"];

  String selectedCategory;

  String name;

  String description;

  double price;

  

  @override
  void initState() {

    super.initState();

    selectedCategory = widget.product.category;

    product = widget.product;

    

    pictureWidget = PictureWidget();


      img = widget.img;

      setState(() {
        
      });

      
  


  }

  @override
  void dispose() {

    super.dispose();

 

    img = null;

    imageCache.clear();

    PictureWidget.img = null;
  }

 



  









  @override
  Widget build(BuildContext context) {

          PictureWidget.img = img;



    return Scaffold(

      appBar: AppBar(title: Text(AppLocalizations.of(context).translate("update_product"),)),

      body: SingleChildScrollView(

        scrollDirection: Axis.vertical,
        
        child: Column(children: [

        pictureWidget,

        Align(alignment: Alignment.centerLeft, child: buildDropdownMenu()),

        Form(

          key: _formKey,

          autovalidateMode: AutovalidateMode.always,

          child: Column(

            children: [

              TextFormField(onChanged: (value) => product.name = value,
              decoration: InputDecoration(labelText: AppLocalizations.of(context).translate("product_name")),
             
              initialValue: widget.product.name,


              ),

              TextFormField(onChanged: (value) => product.description = value,
              decoration: InputDecoration(labelText: AppLocalizations.of(context).translate("product_description")),
              validator: (value) => validator.descriptionValidator(value,context),
              initialValue: widget.product.description,

              ),

              TextFormField(onChanged: (value) => product.price = double.tryParse(value),
              decoration: InputDecoration(labelText: AppLocalizations.of(context).translate("product_price")),
              validator: (value) => validator.priceValidator(value, context),
              initialValue: widget.product.price.toStringAsFixed(2),


              )


            ],
          ),
        ),

        Row(children: [

          ElevatedButton(onPressed: () {

            Navigator.pop(context);


          }, child: Text(AppLocalizations.of(context).translate("cancelBtn")),
          style: ElevatedButton.styleFrom(primary: Colors.grey),),

          ElevatedButton(onPressed: () async {

            if(_formKey.currentState.validate() == true) {

              product.picture = PictureWidget.img.readAsBytesSync();

              product.category = selectedCategory;

              await SQLiteDbProvider.db.updateProduct(product);

                 setState(() {
      
          });

          

              Navigator.pop(context);


          
          
          
          }

          else {

            ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text(AppLocalizations.of(context).translate("check_input"))));
          }


          }, child: Text(AppLocalizations.of(context).translate("updateBtn")),
          style: ElevatedButton.styleFrom(primary: Colors.green),)
        ],)


      ],),
      
    ));
  }

    Widget buildDropdownMenu() {


    return DropdownButton(

      hint: Text(AppLocalizations.of(context).translate("choose_category")),
      value: selectedCategory,
      onChanged: (value) {

        setState(() {
          
          selectedCategory = value;
        });
      },

      items: categories.map((_category) {

        return DropdownMenuItem(

          child: Text(_category),
          value: _category
        );


      }).toList()


    );
  }



 
}
