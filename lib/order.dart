class Order {

  int id;

  double price;

  int userId;

  String state;

  String deliveryMethod;

  String dateTime;


  Order(this.id, this.price, this.userId,  this.state, this.deliveryMethod, this.dateTime);

  factory Order.fromMap(Map<String,dynamic> data) {

    return Order(

      data['id'],
      data['price'],
      data['userId'],
      data['state'],
      data['deliveryMethod'],
      data['dateTime']
    );




  }

  Map<String, dynamic> toMap() => {

    "id": id,
    "price": price,
    "userId": userId,
    "state": state,
    "deliveryMethod": deliveryMethod,
    "dateTime": dateTime


  };

  @override
  String toString() {

    return "Order id: " + id.toString() + " Order price: " + price.toString() + " User id " + userId.toString() + " State: " + state + " Delivery method: " + deliveryMethod + " DateTime " + dateTime;
  }

  

}