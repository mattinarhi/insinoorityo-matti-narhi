import 'package:flutter/material.dart';
import 'package:inssityo/appLocalization.dart';
import 'package:inssityo/cartProvider.dart';
import 'package:inssityo/makeOrderView.dart';
import 'package:inssityo/product.dart';
import 'package:inssityo/productDetailScreen.dart';

import 'package:provider/provider.dart';

class ShoppingCart extends StatefulWidget {


final String username;

ShoppingCart(this.username);



  @override
  _ShoppingCartState createState() => _ShoppingCartState();
}

class _ShoppingCartState extends State<ShoppingCart> {

  Product product = new Product(2, "Shoe brush", "Brush for dance shoes", "Other", 5.00, null);


  @override

  void initState() {

    super.initState();






  }


  @override
  Widget build(BuildContext context) {


 
  
      

    

       return Scaffold(

      appBar: AppBar(title: Text(AppLocalizations.of(context).translate("shopping_cart")),
   
      automaticallyImplyLeading: false,
      actions: [IconButton(icon: Icon(Icons.delete),
      onPressed: () {

        if(Provider.of<CartProvider>(context, listen: false).cartSize == 0) {

        Provider.of<CartProvider>(context, listen: false).emptyCart(context);
        ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text(Provider.of<CartProvider>(context, listen: false).emptyCartMsg)));
        }
        else {

          showAlertDialog(context);
        }

      },),
      
      TextButton(onPressed: () async {

        List<Product> products = Provider.of<CartProvider>(context, listen: false).shoppingcart;

        double price = Provider.of<CartProvider>(context, listen: false).cartPrice;

        int size = Provider.of<CartProvider>(context, listen: false).cartSize;

        if(Provider.of<CartProvider>(context, listen: false).cartSize == 0) {

          ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text(AppLocalizations.of(context).translate("empty_order"))));
        }

        else {

       final result = await Navigator.push(context, MaterialPageRoute(builder: (context) => MakeOrderView(widget.username,products, price, size)));

       if(result == true) {

         Provider.of<CartProvider>(context, listen: false).emptyCart(context);
       }

        }

      },
      child: Text(AppLocalizations.of(context).translate("order"), style: TextStyle(color: Colors.white),),)],),
      resizeToAvoidBottomInset: false,

      body: Container(

        child:


   
        
        Consumer<CartProvider>(

          

          builder: (context, provider, child) {


        

      return  Column(

        children:[

        Text(provider.cartSize == 0 ? "" : AppLocalizations.of(context).translate("total_price") + ": " + provider.cartPrice.toStringAsFixed(2) +" €", style: TextStyle(fontSize: 20.0, fontWeight: FontWeight.bold),),
        
        
        Expanded(child: ListView.builder(

          itemCount: provider.cartSize == 0 ? 1 : provider.shoppingcart.length,
          
          
          itemBuilder: (context, index) {

            if(provider.cartSize != 0) {


              return Column(
                
                children: [

                Card(
                  child: InkWell(

                    onTap: () {

                      Navigator.push(context, MaterialPageRoute(builder: (context) => ProductDetailScreen(provider.shoppingcart[index])));

                    },

                    child: Column(

                      children: [

                        Container(

                          height: 150,
                          width: MediaQuery.of(context).size.width,
                          child: Image.memory(provider.shoppingcart[index].picture),
                        ),

                        Text(provider.shoppingcart[index].name, style: TextStyle(fontSize: 20.0, fontWeight: FontWeight.bold,)),

                        Text(AppLocalizations.of(context).translate("price") + ": " + provider.shoppingcart[index].price.toStringAsFixed(2) + " €", style: TextStyle(fontSize: 20.0, fontWeight: FontWeight.bold),),

                        Text(AppLocalizations.of(context).translate("amount") + ": " + provider.shoppingcart[index].itemCount.toString(), style: TextStyle(fontSize: 20.0, fontWeight: FontWeight.bold),),

                       Row(children:[

                         ElevatedButton(onPressed: () {

                           provider.addProduct(provider.shoppingcart[index]);

                         }, child: Text("+"),
                         style: ElevatedButton.styleFrom(primary: Colors.green),),
                         
                         
                         
                         
                         ElevatedButton(onPressed: () {

                          provider.deleteProduct(provider.shoppingcart[index]);

                        }, child: Text("-"),
                        style: ElevatedButton.styleFrom(primary: Colors.red))





                      ],
                    ),
                      ])
                ),


                
                
              )]);
            }

            else {

              return Center(child: Text(AppLocalizations.of(context).translate("no_products")));
            }



        }
        
        
        
        
        
        ))]);



          }),
      
        ),
    );
      
  }

  showAlertDialog(BuildContext context) {

    AlertDialog alertDialog = AlertDialog(

      title: Text(AppLocalizations.of(context).translate("warning")),
      content: Text(AppLocalizations.of(context).translate("empty_cart")),

      actions: [

        TextButton(

          child: Text(AppLocalizations.of(context).translate("cancelBtn")),

          onPressed: () {

            Navigator.of(context).pop();
          },
        ),

        TextButton(

          child: Text(AppLocalizations.of(context).translate("yesBtn")),

          onPressed: () {

            Navigator.of(context).pop();
            Provider.of<CartProvider>(context, listen: false).emptyCart(context);

            ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text(Provider.of<CartProvider>(context, listen: false).emptyCartMsg)));
          },


        )
      ],

      
    );

    showDialog(context: context, builder: 
    
    (BuildContext context) {

      return alertDialog;
    });



  }
  
    
      
      }

